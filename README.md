# SADE USSD Framework


## How to run this Project

Clone the project from git

## Install the dependencies and run 
npm install 


open the file "public/Main.js" and replace "win.loadURL(`file://${path.join(__dirname, '../build/index.html')}`)" with "win.loadURL('http://localhost:3000/')"

## Launch React 
npm run start

## Launch the electron browser, which will connect to port 3000
npm run electron-start

## To build, run 
"npm run build"

This  will build the react app first, then budle it inside electron, and spit out a "Sade Setup 0.0.1.exe" file in the dist folder. 

Do not forget to replace "win.loadURL('http://localhost:3000/')" with "win.loadURL(`file://${path.join(__dirname, '../build/index.html')}`)" 
in public/Main.js, before building

# HAPPY BUILDING

/* Copyright (C) Ptrikey, Inc - All Rights Reserved
 * Unauthorized copying of this any file in this repository, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Orogun Samuel <samuel@ptrikey.com>, March 2021
 */

