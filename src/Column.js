import React,{useState} from 'react';
import LineTo from './react-lineto';
import { Line } from './react-lineto';

function Column ({upd, cols, index, optIndex, drgObj, setDrgObj}){
		//const [options,setOptions] = useState([""]);
		//console.log(cols)
		const addItem = ()=> {
					const newCols = [...cols]; newCols[index].opt[optIndex].flows.push({flowChildColumn: null,flowChildIndex:null, flowText:"", flowRangeStart:"", flowRangeStop:""}); upd(newCols);  } 
		const myOpt = cols[index].opt[optIndex]
		const myOptFlows = myOpt.flows
		return(
			
			<React.Fragment>
			
			{myOptFlows.map((option,i) => (
				<React.Fragment key={i}>
				{ (()=>{let flowObj = myOptFlows[i];
						let rowCss = index+"s"+optIndex+"d"+i;
						if (drgObj.startElem ==rowCss && drgObj.x1 > 0){
							return <Line x0={drgObj.x0} y0={drgObj.y0} x1={drgObj.x1-10} y1={drgObj.y1-10} borderWidth="0.07rem" borderColor="#ea9013"/> 
						}
						else if (flowObj.flowChildIndex !== null){
							//console.log("plotting from "+index+"s"+optIndex+"d"+i+" to "+flowObj.flowChildColumn+"s"+flowObj.flowChildIndex)
							return <LineTo from={index+"s"+optIndex+"d"+i} to={flowObj.flowChildColumn+"s"+flowObj.flowChildIndex}
							fromAnchor="center right" toAnchor="center left" delay="10" borderColor="#00f775" borderWidth="0.07rem"/>
						}
					}) ()
                }

				<div style={{flexGrow:"1", display:"flex", flexDirection:"column", marginBottom:"0px"}} >
					<div style={{ display:"flex", alignItems:"center"}} className={index+"s"+optIndex+"d"+i}>
						{myOpt.flowType==="menu" ?	<input type="text" value={i+1} onChange={(e)=>{}} placeholder="hotkey" style={{maxWidth:"1.2rem",color:"black",fontWeight:"bold",backgroundColor:"#c1c1c1",textAlign:"center",border:"1px solid #989191",borderBottom:(i+1)===myOptFlows.length?"1px solid #989191":"none",fontSize:"0.75rem"}}/> : null }
						
						{myOpt.flowType==="amount" ? 
							(<React.Fragment> <input type="text" size="8" placeholder ="from(inclusive)" style={{padding:"0",fontSize:"0.75rem"}} value = {option.flowRangeStart}
								onChange = {(event)=>{ let etv = event.target.value.trim() ; let etvNum = parseInt(etv); const newCols = [...cols]; 
									 newCols[index].opt[optIndex].flows[i].flowRangeStart = etv.length===0?"" : (!isNaN(etvNum)? etvNum+"" : option.flowRangeStart ); upd(newCols);
								}}/> - 
								<input type = "text" placeholder= "to (inclusive)" style={{padding:"0" , marginRight:"0.4rem",fontSize:"0.75rem"}} size="8" value = {myOptFlows[i].flowRangeStop}
								onChange = {(event)=>{ let etv = event.target.value.trim() ; let etvNum = parseInt(etv); const newCols = [...cols]; 
									 newCols[index].opt[optIndex].flows[i].flowRangeStop = etv.length===0?"" : (!isNaN(etvNum)? etvNum+"" : option.flowRangeStop ); upd(newCols);
								}}/>
							</React.Fragment>) : 
							(myOpt.flowType==="menu") ? ( 
							<input type="text" size="10" value={option.flowText} placeholder="option" style={{flexGrow:"1",fontSize:"0.75rem",border:"1px solid #989191",borderLeft:"none",borderBottom:(i+1)===myOptFlows.length?"1px solid #989191":"none", paddingLeft:"0.3rem",backgroundColor:"#e6e3e3"}} onChange={(event)=>{ 
								let etv = event.target.value.trim();
								if (!myOptFlows.find(t => t===etv) && etv !==""){
									const newCols = [...cols]; 
									const currentFlow = newCols[index].opt[optIndex].flows[i];
									currentFlow.flowText= etv; 
									/*if (currentFlow.flowChildColumn !== null){
										console.log(currentFlow)
										let childOpt = newCols[currentFlow.flowChildColumn].opt[currentFlow.flowChildIndex];
										childOpt.flowHeader.forEach((parentAdress, v)=>{
											newCols[parentAdress.colPosition].opt[parentAdress.optIndex].flows[parentAdress.optTextIndex].flowText = etv;
											// set the value of the parent flows to this text.
										});
									}*/
									upd(newCols)
								}		
							}} />) : null 
						}
						<input type="button" value="next stage =>" style={{flexGrow:"1",border:"1px solid #989191",borderBottom:(i+1)===myOptFlows.length?"1px solid #989191":"none", borderLeft:"0", fontWeight:"bold",backgroundColor:"#c1c1c1",fontSize:"0.75rem"}} 
						onClick={()=> {
							//alert(index +" "+cols.length)
							if (index+1===cols.length){
								let nOpt = {
									flowHeader:[{colPosition:index, optIndex: optIndex , optTextIndex: i}],stageName:"", quickCode:"", ins:"",flowType:"", hook:"", pinSetinPrevStage:false,amountInputStage:0,textInputStage:0,
									flows:[{flowChildColumn : null,flowChildIndex:null, flowText:"",flowRangeStart:"", flowRangeStop:""}] , pos:1
								}
								nOpt.pinSetinPrevStage = myOpt.flowType==="pin" ? true : myOpt.pinSetinPrevStage;
								nOpt.amountInputStage = myOpt.flowType==="amount" ? index : myOpt.amountInputStage;
								nOpt.textInputStage = myOpt.flowType==="text" ? index : myOpt.textInputStage;
								nOpt.stageName =  myOptFlows[i].flowText !== "" ? myOptFlows[i].flowText.split(' ').join('')+ "_stage" : ""

								const newCols = [...cols, {optCount:1, opt: [nOpt] }];
								newCols[index].opt[optIndex].flows[i].flowChildIndex= 0
								newCols[index].opt[optIndex].flows[i].flowChildColumn= index+1 // the child is in an adjacent column
								newCols[index+1].optCount++;
								upd(newCols	); 
							}
							else {
								//alert(cols[index].opt);
								//let obj = cols[index].opt.find(o => o.flowHeader === options[i]);
								//alert('obj '+myOptFlows[i].flowChildIndex );
								if(myOptFlows[i].flowChildIndex ===null){
									const newCols = [...cols];
									let nOpt = { 
										flowHeader:[{colPosition:index, optIndex: optIndex , optTextIndex: i}],stageName:"", quickCode:"", ins:"", flowType:"",hook:"",pinSetinPrevStage:false,amountInputStage:0,textInputStage:0,
										flows:[{flowChildColumn : null, flowChildIndex:null, flowText:"",flowRangeStart:"", flowRangeStop:""}], pos : newCols[index+1].optCount
									}
									nOpt.pinSetinPrevStage = myOpt.flowType==="pin" ? true : myOpt.pinSetinPrevStage;
									nOpt.amountInputStage = myOpt.flowType==="amount" ? index : myOpt.amountInputStage;
									nOpt.textInputStage = myOpt.flowType==="text" ? index : myOpt.textInputStage;
									nOpt.stageName =  myOptFlows[i].flowText !== "" ? myOptFlows[i].flowText.split(' ').join('')+ "_stage" : ""

									newCols[index].opt[optIndex].flows[i].flowChildIndex= newCols[index+1].optCount-1;
									newCols[index].opt[optIndex].flows[i].flowChildColumn= index+1 // the child is inn the next column

									newCols[index+1].optCount++;
									newCols[index+1].opt.push(nOpt);
									upd(newCols	); 
							 	}
							}
						}} />
						<input type="button" value="" draggable = {true} style={{borderRadius:"50%", width:"0.7rem", height:"0.7rem", marginLeft:"0.1rem", border:"none", fontWeight:"bold",backgroundColor:"#c1c1c1"}} 
							onDragStart={(e)=>{const nDrgObj = {...drgObj};
								nDrgObj.x0 =  e.pageX; nDrgObj.y0= e.pageY;	setDrgObj(nDrgObj); 
								//console.log("start drg obj "+nDrgObj.x0+","+nDrgObj.y0) 
							}}

							onDragEnd = {(e)=>{
								const nDrgObj = {...drgObj}; nDrgObj.x1 =  0; nDrgObj.y1= 0; setDrgObj(nDrgObj); 
								//console.log("end drg obj "+nDrgObj.startElem+" x1: "+nDrgObj.x1+" y1: "+nDrgObj.y1+" stopelem: "+ nDrgObj.stopElem)
								let poses = nDrgObj.stopElem.split("s"); 
								let childColPosition =  parseInt(poses[0]) ; let optPosition =  parseInt(poses[1])
								let pObj = nDrgObj.startElem.split("s"); 
								let parentColPosition = parseInt(pObj[0]); let parentOptPosition = pObj[1].split("d")[0];
								
								if (parentColPosition < childColPosition){
									nDrgObj.stopElem = ""; nDrgObj.startElem=""; // these two must not be equal
									const newCols = [...cols];
									newCols[index].opt[optIndex].flows[i].flowChildIndex = optPosition; // indicate that this flow has a child
									newCols[index].opt[optIndex].flows[i].flowChildColumn = childColPosition; 
									//console.log(newCols[index].opt[optIndex].flows[i])
									let parentFlowAdress = {colPosition: index,optIndex : optIndex, optTextIndex : i};
									newCols[childColPosition].opt[optPosition].flowHeader.push( parentFlowAdress ); // register the parent to the child
									upd(newCols	);
								}else {
									alert("you cannot drag and drop to the same node");
								}
							}}

							onDrag={(e)=> { const nDrgObj = {...drgObj};
								nDrgObj.startElem = index+"s"+optIndex+"d"+i; nDrgObj.x1 =  e.pageX; nDrgObj.y1= e.pageY;
								setDrgObj(nDrgObj);  }} />
					</div>
		 
  				</div>
  				</React.Fragment>
				))
			}
			{myOpt.flowType === "menu" ?  <input type="button" value="+ Item" style={{marginTop:"10px",fontSize:"0.75rem",fontFamily:"SegoeUI, sans-serif",backgroundColor:"#4a4a4a",color:"white",border:"none",borderRadius:"0 0 0.2rem 0.2rem",letterSpacing:"0.05rem"}} onClick={addItem}/>
      		:  ( myOpt.flowType === "amount" ? <input type="button" value="+ Range" style={{marginTop:"10px",fontSize:"0.75rem",fontFamily:"SegoeUI, sans-serif",backgroundColor:"#4a4a4a",color:"white",border:"none",letterSpacing:"0.05rem",borderRadius:"0 0 0.2rem 0.2rem"}} onClick={addItem}/> : null )
      	}
      		</React.Fragment>
	);

	
}

export default Column