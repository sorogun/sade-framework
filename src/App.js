import React, {useState,useEffect} from 'react';
import logo from './logo.svg';
import Column from './Column';
import Opt from "./Opt";
import SideFile from "./SideFile";
import PlayButton from './PlayButton';
import BuildButton from './BuildButton';
import Modal from './Modal';
import Ide from './Ide';

const fs = window.require('fs');  // this is a native api, so window is needed before require.
const path = window.require('path');
const clone = require('rfdc')(); // this is no a native api, so window is not needed
const childProcess = window.require('child_process');

var timer; var hTimer;
const rootDir = '/sade-ussd/'
const initText = '//do-not-delete-this-line-sade-init';
const exitText = '//do-not-delete-this-line-sade-exit';
const codeWrapper = {pre:"function (input,stages) { \n let error = { val:false , msg:'' , terminal:false };\n "+initText,
          post:exitText+" \n return error;\n}"}
const  importStart = "//do-not-delete-this-line-import-start"
const  importEnd = "//do-not-delete-this-line-import-end"
//optTextIndex starts from 0 changing this to 1 will affect buildPolyfill function
const defaultColumn = [ {opt: [ {flowHeader:[{colPosition: null , optIndex:0, optTextIndex:0}], stageName:"starting_point", quickCode:"",ins:"", flowType:"menu", hook:"", pos:1, pinSetinPrevStage:false, amountInputStage:0, textInputStage:0, flows:[{flowChildColumn: null , flowChildIndex:null, flowText:"", flowRangeStart:"", flowRangeStop:""}] }], optCount:1 } ];
const freshText = '<ussd-ussd>'
let session = 'closed';
let prevScreen = '';

function App() {
  const [columns,setColumns] = useState(defaultColumn);
  const [drg,setDrag] = useState({startElem:"",x0:0, y0:0, x1:0, y1:0, stopElem:""});
  const [props, setProps] = useState({project:"", devEnv: {lang:""},rootCode:"",mode:"design", u_type:"normal_user", modal:{isOpen:false}, phone:{isOpen:false}, buildDialog:{isOpen:false}, i_deps:false, start:{prj_title:"", urc:"", error:"",de:{lang:""} }})
  const [coding, setCoding]= useState ({imports:"",first_time_user:"",normal_user:"",skeleton:"", hooks:"", override:"", tab:'normal_user.js'})  
  const [terminal,setTerminal] = useState({pn:'444446',uid:'20585514267',apk:'100000000',point:'start',imei:'bike',sc:'',proc:childProcess.spawn('ls')} )
  
  /*  these properties are put here because the on data function of process keeps ovewriting 
    state to it's default value when one property needs to be updated. if any of these properties are
    overwritten, it does not lead to a bug, so they are separated from the other properties in terminal*/
  const [tinker,setTinker]=useState({screen:'',user_input:'',error:false})
  
  /* by design, the project has to be passed in as an argument, to deal with cases where the user has selected the project under
  his list of 'active projects', so the 'project' property under props has not been set. This fdunction also takes
  care of settign that property, amongst other 'props' properties' */
  
  const installDeps = (project)=>{
    // installs the js dependencies needed for the ussd app
    const platform = window.navigator.platform.toLowerCase();
    const nppm = /^win/.test(platform) ? 'npm.cmd' : 'npm';
    childProcess.spawn(nppm,['install','--prefix' ,rootDir+project,'axios'] ,{ encoding: 'utf-8' });
    childProcess.spawn(nppm,['install','--prefix' ,rootDir+project,'inquirer'] ,{ encoding: 'utf-8' });
    childProcess.spawn(nppm,['install','--prefix' ,rootDir+project,'yargs'] ,{ encoding: 'utf-8' });
  }

  const refreshCode = (project,columns,mode)=>{
  if (fs.existsSync(rootDir+project+'/case.sade') ){
    //console.log('code refresh called '+props.u_type);
    const caseFile = fs.readFileSync(rootDir+project+'/case.sade', 'utf8');
    const truckFile = fs.readFileSync(rootDir+project+'/'+mode+'/'+props.u_type+'/truck.sade', 'utf8');
    const hookFile = fs.readFileSync(rootDir+project+'/'+mode+'/'+props.u_type+'/Hooks.js', 'utf8');
    //console.log('cf\n'+caseFile);
    let caseObj = JSON.parse(caseFile);
    let truckObj = JSON.parse(truckFile);
    let csCols = caseObj.columns;
    const iStart = hookFile.indexOf(importStart);
    const iEnd = hookFile.indexOf(importEnd,iStart);
    let imps = (iStart<0 || iEnd<0 || iStart>iEnd ) ?"" :  hookFile.substring(iStart+importStart.length,iEnd);
      
    truckObj.map((col,v)=> col.opt.map((opt,w)=>{
      const level = 'hook['+v+']['+w+']' 
      var hookPoint = hookFile.indexOf(level) ;
      const initPoint = hookFile.indexOf(initText,hookPoint);
      const exitPoint = hookFile.indexOf(exitText,hookPoint);
      const code = hookFile.substring(initPoint+initText.length,exitPoint);
      //console.log(level+" "+hookPoint+" "+initPoint+" "+exitPoint+" "+code);
      opt.hook = code;
      //console.log(opt.hook+"code");

    }));
    
    const nProps = {...props};
    nProps.rootCode = caseObj.rootCode;
    nProps.devEnv = caseObj.devEnv;
    nProps.project = caseObj.project;
    //nProps.u_type = caseObj.u_type; this will lead to a bug, in which the u_type variable doesnt change, so not implemented yet
    
    const nCoding = {...coding};
    nCoding.imports = imps; /* the value of this imports here depends on your hookfile */
    setProps(nProps);
    setCoding(nCoding);
    setColumns(truckObj);
    
    //console.log('immediately after '+JSON.stringify(columns))
  }
}
  const filterText =  (unfiltered)=>{
    const badSegment = ''
    let filtered = 
    unfiltered.split('\n').map((unf,i)=> unf.includes(badSegment) ? '' :  unf.trim() ).join('\n')
    //if (unfiltered.includes(badSegment))  return unfiltered.substring(0,unfiltered.indexOf(badSegment))
    let oktext = filtered.includes(freshText)  
                  ? filtered.substring(filtered.lastIndexOf(freshText)+freshText.length) 
                      :  filtered
    
    return [oktext];
  }
const exportCodeFs = (project, columns,mode) =>{
  if (project !== "" ) {
    //console.log('code export called '+props.u_type);
    let c = buildHooks(columns);
    //console.log('exported code \n'+c+' export fin');

    fs.writeFileSync(rootDir+project+'/'+mode+'/'+props.u_type+'/Hooks.js',c,'utf8',function (err) { if (err) return alert(err);} )
    let columnCase = clone(columns);
    columnCase.forEach((col,v)=> col.opt.forEach((opt,w)=>{
      opt.hook=''; // no need persisting the code with the rest of the state
    })); 
    /* this method of creating objects is okay because the value pairs are not nested objects 
      If they were nested objects, we'd have to clone them
    */
    let propCase = {project:project, devEnv: props.devEnv,rootCode:props.rootCode}
    const pCaseText = JSON.stringify(propCase);
    const truckText = JSON.stringify(columnCase);
    if (!pCaseText.trim().startsWith('0000')){
      fs.writeFileSync(rootDir+project+'/case.sade', pCaseText,'utf8', function (err) { if (err) return alert(err);} )
      fs.writeFileSync(rootDir+project+'/'+mode+'/'+props.u_type+'/truck.sade', truckText,'utf8',function (err) { if (err) return alert(err);});
    }else {
      console.log('data corruption on writing to case.sade, it is attempting to write');
      console.log(pCaseText);
      console.log('instead of '+JSON.stringify(propCase))
      //alert('bug found, check console for more info')

    }
  }
}

const buildHooks= (columns)=>{

  let adder="\n"+importStart;
    adder+= coding.imports.startsWith('\n') ? coding.imports : '\n\n'+coding.imports;
    adder+= coding.imports.endsWith('\n') ? importEnd : '\n\n'+importEnd;
    adder+="\n\nlet hook = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]] //js only 20-column 2-dim array declaration \n \n"; 

  columns.forEach((col,v)=> col.opt.forEach((opt,w)=>{
    adder+='\nhook['+v+']['+w+'] = '+codeWrapper.pre;
    adder+=opt.hook.startsWith('\n') ? opt.hook : '\n'+opt.hook;
    adder+= opt.hook.endsWith('\n') ? codeWrapper.post : '\n'+codeWrapper.post;
    adder+= '\n';
  })); 
  adder+="\n\nmodule.exports = hook;";

  return adder;
}

  /*  This takes care of sunching the column hooks in code mode */
  useEffect(() => {  if (props.mode ==="code")refreshCode(props.project,columns,'src')   }, [coding.hooks])

  useEffect(() => {  
    if (props.u_type !=="" ){
      if (!fs.existsSync(rootDir+props.project+'/src/'+props.u_type+'/truck.sade') ) {
        //console.log('truck path '+rootDir+props.project+'/src/'+props.u_type+'/truck.sade')
      //console.log('initiating '+props.u_type)
         /// do nothing, this means the u_type has already been activated
         let defaultColumnClone = clone(defaultColumn); // clone is necessary so that the object is deep copied so as to avoid bugs
         setColumns(defaultColumnClone); // set the column to a default value.
         exportCodeFs(props.project,defaultColumn,'src'); // begin writing to the new file
      }else{
        /* this is not a 1st time user */
        refreshCode(props.project,columns,'src')
      }
    }
  }, [props.u_type])
 
 
  useEffect(() => {    
      window.addEventListener('beforeunload', 
        ()=>{
          //console.log('before unload called ');
          if (props.mode === "code"){
            /* the Hooks.js file is more updated than the state in 'code mode'. so, what we do is parse the HOOKS.js file into the state 
              before exporting calling exportCodeFs.
            */
            refreshCode(props.project,columns,'src')
          }
            exportCodeFs(props.project,columns,'src'); 
          
        //console.log('before unload finished ');
        } ); 
      return window.removeEventListener('beforeunload', console.log('unload removed'));
      
    },[]
  );


  return (
    <React.Fragment>
      <div style={{display:"flex",margin:"0.3rem", minHeight:"1.2rem",alignItems:"center",paddingRight:"8rem"}}>
        {props.project!=="" && props.mode==="design" ?
          <React.Fragment>
            <label style={{textAlign:"left", margin:"0",marginRight:"0.3rem",color:"white",fontSize:"0.9rem"}}> Project : </label>
            <div style={{display:"flex",justifyContent:"center",alignItems:"center"}}>
              <label style={{textAlign:"center", margin:"0",color:"black",fontWeight:"bold",backgroundColor:"white",borderRadius:"0.2rem", padding:"0", paddingLeft:"0.3rem",paddingRight:"0.3rem",fontSize:"0.8rem"}}>{props.project} </label>
            </div>
          </React.Fragment> : null
        }
        {props.project !=="" ?
          <select  value={props.u_type} style={{color:"white",backgroundColor:"transparent",borderRadius:"0.2rem",border:"2px solid white",outline:"none",fontFamily:"SegoeUI,sans-serif",marginLeft:"2rem"}}onChange={(e)=>{ 
                          const nProps= {...props}; nProps.u_type = e.target.value; setProps(nProps); }}>
                <option disabled value="" >--Select an option--</option>
                <option value="first_time_user" style={{color:"black",fontFamily:"SegoeUI,sans-serif"}}> First Time User</option>
                <option value="normal_user" style={{color:"black",fontFamily:"SegoeUI,sans-serif"}}> Normal User </option>
            </select> : null
        }
        <h4 style={{textAlign:"center", margin:"0", color:"white",fontWeight:"normal", flexGrow:"1"}}> Sade Framework</h4>
      </div> 
      { props.project ===""?
      <React.Fragment>
        <div style={{display:"flex"}}>
          <div style={{display:"flex", flexDirection:"column", minWidth:"10rem", marginTop:"3rem"}}>
            <div> <label style={{color:"white"}} onClick={()=>{const nProps = {...props}; nProps.modal.isOpen= true; setProps(nProps);}}> + New Project</label> </div>
            <Modal show={props.modal.isOpen} onClose={()=>{}} extraStyle={{}}>
              <div style={{display:'flex', flexDirection:'column', flexGrow:'1'}}>  
                
                <div style={{flexDirection:"column", flexGrow:"1"}}>
                  <h2> New Project </h2>
                  <div style={{display : props.start.error===""?"none" :"flex", color:"#e08282", marginBottom:"1rem", display:"flex",fontSize:"0.8rem", fontWeight:"bold"}} > 
                    <label>{props.start.error} </label>
                  </div>
                  <div style={{display:"flex", justifyContent:"space-between", marginBottom:"2rem"}}>
                    <label style={{marginRight:'0.5rem'}}>Project Title </label> <input type="text" onChange={
                      (e)=>{
                        const nProps= {...props}; nProps.start.prj_title = e.target.value.replace(/\s+/g, '');                         setProps(nProps);
                      }
                    } value ={props.start.prj_title}/>
                  </div>
                  <div style={{display:"flex", justifyContent:"space-between", marginBottom:"2rem"}}>
                    <label style={{marginRight:'0.5rem'}}>Ussd Root code </label> <input type="text" placeholder="eg. *797#" onChange={
                      (e)=>{
                        const nProps= {...props}; nProps.start.urc = e.target.value.replace(/\s+/g, '');
                        setProps(nProps);
                      }
                    } value ={props.start.urc}/>
                  </div>
                  <div style={{display:"flex", flexDirection:"column", marginBottom:"2rem"}}>
                    <label style={{marginRight:'0.5rem'}}>Dev Environment</label> 
                    <select id="dev" value={props.start.de.lang} onChange={(e)=>{ 
                        const nProps= {...props}; nProps.start.de.lang = e.target.value; setProps(nProps); }}>
                      <option disabled value="" >--Select an option--</option>
                      <option value="js">Javascript</option>
                    </select>
                  </div>
                  
                </div>
                <div style={{}}> 
                    <button type='button' onClick={()=>{
                      const nProps= {...props}
                        
                        if (props.start.prj_title === ""){
                            nProps.start.error= "Please specify a Project Title"; setProps(nProps);
                            return;
                        }

                        if (fs.existsSync(rootDir+props.start.prj_title)){
                             nProps.start.error= "A project with this name already exists"; setProps(nProps);
                             return;
                        }

                         /*
                         The regex expressions below check if the ussd coded is valid. 
                         The first past 
                         1. ensures that it starts with *(/^\*)
                         2. contains only digits and * in between ([\d/*]+)
                         3. ensures that there is a digit before the trailing # (\d#$/)
                          The first part allows multiple asteriks ** to be placed side by side, which is what the 
                          second part prevents
                         */
                         //                test1                                          test2
                        if ( !(    /^\*[\d/*]+\d#$/.test(props.start.urc)    &&     !/\*\*/.test(props.start.urc)   ) ){
                          nProps.start.error= "Invalid USSD Root Code"; setProps(nProps);
                          return;
                        }
                        

                        if (props.start.de.lang === ""){
                          nProps.start.error= "Select a Development Evnvironment"; setProps(nProps);
                          return;
                        }
                        /* This will only run after all other conditions have been fulfilled */
                        fs.mkdirSync(rootDir+props.start.prj_title);
                        fs.mkdirSync(rootDir+props.start.prj_title+'/src/');
                        fs.mkdirSync(rootDir+props.start.prj_title+'/src/normal_user');
                        fs.mkdirSync(rootDir+props.start.prj_title+'/src/first_time_user');
                        fs.mkdirSync(rootDir+props.start.prj_title+'/build');
                        fs.mkdirSync(rootDir+props.start.prj_title+'/build/normal_user',{ recursive: true });
                        fs.mkdirSync(rootDir+props.start.prj_title+'/build/first_time_user');
                        
                        /*  create files  necessary with default text */
                        fs.writeFileSync(rootDir+props.start.prj_title+'/src/normal_user/normal_user.js','','utf8',function (err) { if (err) return alert(err);} )
                        fs.writeFileSync(rootDir+props.start.prj_title+'/src/first_time_user/first_time_user.js','','utf8',function (err) { if (err) return alert(err);} )
                        fs.writeFileSync(rootDir+props.start.prj_title+'/src/Skeleton.js',skeletonSrc(props.start.urc),'utf8',function (err) { if (err) return alert(err);} )
                        installDeps(props.start.prj_title);
                        
                        exportCodeFs(props.start.prj_title,columns,'src');
                
                        
                        nProps.project = props.start.prj_title;
                        nProps.rootCode = props.start.urc;
                        nProps.devEnv.lang = props.start.de.lang;
                        nProps.modal.isOpen= false;
                        setProps(nProps);

                   
                  }}> Next </button>
                    <button type='button' onClick={()=>{const nProps = {...props}; nProps.modal.isOpen= false; setProps(nProps);} }> Back </button>
                </div>
              </div>
            </Modal>
          </div>
          <div style={{display:"flex", flexDirection:"column", flexGrow:"1"}}>
            <label style={{color:"white"}}> Active Projects </label>
              <hr style={{marginRight:"0",marginLeft:0, border:"none", borderTop:"1px solid white"}}/>
              <div style={{display:"flex", flexDirection:"column"}}>{ 
                (()=>{
                if (!fs.existsSync(rootDir) ) fs.mkdirSync(rootDir);
                var pList = fs.readdirSync(rootDir);
                return pList.map((prj,i) => (
                  <label key={i}
                   style={{color:"white", display:"flex", alignItems:"center", paddingLeft:"1rem",backgroundColor:"#6d6868",marginBottom:"0.8rem"}}
                   onClick={()=>{  
                    refreshCode(prj,columns,'src')
                 }}
                  >{prj}</label> ) )
                    
                  })()
              }
              </div>
          </div>
        </div>
      </React.Fragment> :
      <React.Fragment>

        <div style={{display:"flex",position:"absolute",top:"0.5rem",right:"1rem"}}> 
            
            <input type="button" value={props.mode ==="design"? "Compile to Javascript" :"View design"} style={{color:"white",backgroundColor:"#ea9013",border:"none",borderRadius:"0.2rem"}}
              onClick={()=>{
                if (props.mode ==="design"){
                  let b = srcJs(1,columns,"","",0,0,"");
                  let a = buildPolyfill(columns,props.rootCode)
                  let c = buildHooks(columns);
                  let d = buildOverride(rootDir+props.project);
                  let e = skeletonSrc(props.rootCode);

                  const nCoding = {...coding};
                  nCoding[props.u_type]=b;nCoding.hooks=c; nCoding.override=d;
                  setCoding(nCoding);
                  fs.writeFileSync(rootDir+props.project+'/src/'+props.u_type+'/'+props.u_type+'.js',b.concat(a),'utf8',function (err) { if (err) return alert(err);} )
                  fs.writeFileSync(rootDir+props.project+'/src/Override.js',d,'utf8',function (err) { if (err) return alert(err);} )
                  fs.writeFileSync(rootDir+props.project+'/src/Skeleton.js',e,'utf8',function (err) { if (err) return alert(err);} )
                        
                  // takes care of persisting your hooks and states to Fs .
                  exportCodeFs(props.project,columns,'src');
                  const nProps = {...props}; nProps.mode= "code"; setProps(nProps); 
                }else {
                  const nProps = {...props}; nProps.mode= "design"; setProps(nProps); 
                  //refreshCode(props.project,columns)
                }
              }}/>

              <PlayButton style={{width:"0.6rem", marginLeft:"1rem",border:"1px solid white",display:"flex",flexDirection:"column",justifyContent:"center",paddingLeft:"0.2rem",paddingRight:"0.2rem"}} visible = {true} onClick={()=>{
                
                if (props.mode ==="design"){
                  let b = srcJs(1,columns,"","",0,0,"");
                  let a = buildPolyfill(columns,props.rootCode)
                  let c = buildHooks(columns);
                  let d = buildOverride(rootDir+props.project);
                  let e = skeletonSrc(props.rootCode);

                  const nCoding = {...coding};
                  nCoding[props.u_type]=b;nCoding.hooks=c; nCoding.override=d;
                  setCoding(nCoding);
                  fs.writeFileSync(rootDir+props.project+'/src/'+props.u_type+'/'+props.u_type+'.js',b.concat(a),'utf8',function (err) { if (err) return alert(err);} )
                  fs.writeFileSync(rootDir+props.project+'/src/Override.js',d,'utf8',function (err) { if (err) return alert(err);} )
                  fs.writeFileSync(rootDir+props.project+'/src/Skeleton.js',e,'utf8',function (err) { if (err) return alert(err);} )
                        
                  // takes care of persisting your hooks and states to Fs .
                  exportCodeFs(props.project,columns,'src');
                 
                }


                const nProps = {...props}
                nProps.phone.isOpen = true;
                setProps(nProps);
                const nTerminal = {...terminal};
                const nTinker = {...tinker};
                nTinker.user_input=''; nTinker.screen=''; nTinker.error=false; nTerminal.point='start'; setTinker(nTinker);
                setTerminal(nTerminal);
                //console.log('terminal set to start')
                
             }}/>
             
            <BuildButton style={{width:"0.9rem",marginLeft:"0.4rem",border:"1px solid white", paddingLeft:"0.1rem",paddingRight:"0.1rem"}} visible = {true} onClick = {()=>{
                  let b = buildJs(1,columns,"","",0,0,"");
                  let a = buildPolyfill(columns,props.rootCode)
                  let c = buildHooks(columns);
                  let d = buildOverride(rootDir+props.project);
                  let e = skeletonBuild(props.rootCode);

                  fs.writeFileSync(rootDir+props.project+'/build/'+props.u_type+'/'+props.u_type+'.js',b.concat(a),'utf8',function (err) { if (err) return alert(err);} )
                  fs.writeFileSync(rootDir+props.project+'/build/Override.js',d,'utf8',function (err) { if (err) return alert(err);} )
                  fs.writeFileSync(rootDir+props.project+'/build/Skeleton.js',e,'utf8',function (err) { if (err) return alert(err);} )
                        
                  // takes care of persisting your hooks and states to Fs .
                  exportCodeFs(props.project,columns,'build');
                  const nProps = {...props}
                  nProps.buildDialog.isOpen = true;
                  setProps(nProps);
                 
            }} />
            <Modal show={props.buildDialog.isOpen} onClose={()=>{}} extraStyle={{margin:"auto"}}>
              <div style={{display:'flex', flexDirection:'column',  flexGrow:'1',maxWidth:"15rem"}}>
                <h2 style={{flexGrow:"1"}}>Your project has been built !</h2>
                <div style={{display:"flex", flexDirection:"column", flexGrow:"3"}}>
                  <label> build folder location :-</label> <label style={{fontWeight:"bold"}}>{path.resolve('/sade-ussd/'+props.project+'/build')}</label>
                </div>
                
              </div>
              <button type="button" onClick={()=>{ 
                  const nProps = {...props}; 
                  nProps.buildDialog.isOpen = false; 
                  setProps(nProps);}}
                    >dismiss
              </button>
            </Modal>

             <Modal show={props.phone.isOpen} onClose={()=>{}} extraStyle={{padding:'3px'}}>
                <div style={{display:"flex", flexDirection:"column", flexGrow:"1",justifyContent:"center",backgroundColor:'#b3b2b2'}}>
                  {terminal.point !=='active' ?
                    <div style={{display:'flex', flexDirection:'column', flexGrow:'1',backgroundColor:'white'}}>
                        <div style={{display:'flex',flexDirection:'column',flexGrow:'1',justifyContent:'center'}}>
                          <label style={{marginBottom:'2rem', fontWeight:'bold'}}> Settings </label>
                          <label style={{color:'grey',fontSize:"0.8rem"}}>User ID </label>
                          <input type = 'text' placeholder = 'userid'  style={{marginBottom:'1rem', border:'none',borderBottom:'1px solid black',outline:'none'}} value = {terminal.uid} onChange={(e)=>{ 
                            const nTerminal = {...terminal};
                            nTerminal.uid = e.target.value;
                            setTerminal(nTerminal)
                          }}/>
                          <label style={{color:'grey',fontSize:"0.8rem"}}>Phone number </label>
                          <input type='text' placeholder = 'phonenumber' style={{marginBottom:'1rem', border:'none',borderBottom:'1px solid black',outline:'none'}} value={terminal.pn} onChange={(e)=>{ 
                            const nTerminal = {...terminal};
                            nTerminal.pn = e.target.value;
                            setTerminal(nTerminal)
                          }}/>
                          <label style={{color:'grey',fontSize:"0.8rem"}}>IMEI </label>
                          <input type = 'text' placeholder = 'imei' style={{marginBottom:'1rem', border:'none',borderBottom:'1px solid black',outline:'none'}} value={terminal.imei} onChange={(e)=>{ 
                            const nTerminal = {...terminal};
                            nTerminal.imei = e.target.value;
                            setTerminal(nTerminal)
                          }}/>
                          <label style={{color:'grey',fontSize:"0.8rem"}}>Api key </label>
                          <input type = 'text' placeholder = 'api_key' style={{marginBottom:'1rem', border:'none',borderBottom:'1px solid black',outline:'none'}} value={terminal.apk} onChange={(e)=>{ 
                            const nTerminal = {...terminal};
                            nTerminal.apk = e.target.value;
                            setTerminal(nTerminal)
                          }}/>
                          <label style={{color:'grey',fontSize:"0.8rem"}}>ShortCode </label>
                          <input type = 'text' placeholder = 'short_code' style={{marginBottom:'1rem', border:'none',borderBottom:'1px solid black',outline:'none'}} value={terminal.sc} onChange={(e)=>{ 
                            const nTerminal = {...terminal};
                            nTerminal.sc = e.target.value;
                            setTerminal(nTerminal)
                          }}/>
                          
                        </div>
                        <div style={{display:'flex'}}>
                          <button type='button' style ={{flexGrow:'1', border:'none'}} 
                            onClick={()=>{
                const prcs = childProcess.spawn('node',[rootDir+props.project+'/src/Skeleton.js','--ph='+terminal.pn,'--id='+terminal.uid,'--key='+terminal.apk,'--imei='+terminal.imei,'--ussd='+terminal.sc] /*--ph=444446 --id=20585514267 --key=100000000 --imei=bike'*/  ,{ encoding: 'utf-8' });  // the default is 'buffer'
                prcs.stdin.setEncoding('utf-8');    
                prcs.stdout.setEncoding('utf-8');
                const nTerminal = {...terminal};
                nTerminal.point = 'active'
                session='open'; // open the session
                
                prcs.stdout.on('data', (data)=> {
                  /* DO not update state in this method, it does not use the latest value 
                    and the operation {...whateverstate} will reset all properties in 'whateverstate'
                    to stale values and lead to data corruption. the only state updated here it 'tinker'
                    and data corruption here has no serious consequences.
                  */
                      const pf = filterText(data.toString())
                      prevScreen = pf[0].length>0 ? pf[0] : prevScreen ;
                      setTinker({screen:prevScreen, user_input:'',error:false});
                });

                prcs.on('close', (code) => {
                  session= "closed";
                  prevScreen = "";
                });

                prcs.stderr.on('data', (data) => {
                  const pf = filterText(data.toString())
                  setTinker({screen:pf, user_input:'',error:true});
                });
                
                nTerminal.proc =prcs; 
                setTerminal(nTerminal);
                
                }}>dial </button>
                          <hr style={{margin:'0'}}/>
                          <button type='button' style ={{flexGrow:'1', border:'none'}} onClick={()=>{ 
                            const nProps = {...props}
                            nProps.phone.isOpen = false;
                            setProps(nProps);
                          }}>close </button>
                        </div>
                      </div>

                       :
                    <div style={{backgroundColor: 'white',margin: '0.3rem',  borderRadius: '0.4rem'}}>
                    <div style={{padding:'1rem'}}>
                    {tinker.error?<h2 style={{color:'#ef0101'}}>  Error  </h2> : null }
                      <pre>
                        <div style={{minWidth:"10rem",fontFamily:'Segoe UI,sans-serif',fontSize:'0.8rem',whiteSpace: tinker.error?'normal':null}} contentEditable>
                        {tinker.screen}
                        </div>
                      </pre>
                      {
                          !tinker.error ? <input type="text" placeholder="your response" value={tinker.user_input} style={{border:'none',borderBottom:'1px solid #9c9999',outline:'none'}} onChange={(e)=>{
                          const nTinker = {...tinker};
                          nTinker.user_input = e.target.value;
                          setTinker(nTinker);
                        }}/> : null 
                      }
                    </div>
                    <div style={{display:'flex',justifyContent:'center',padding:'0.1rem'}}>
                      <button type='button' style={{border:'none',backgroundColor:'white',flexGrow:'1',outline:'none',fontFamily: 'Segoe UI,sans-serif'}} onClick={()=>{ 
                          const nProps = {...props}
                          nProps.phone.isOpen = false;
                          setProps(nProps);
                      }}>Cancel </button>
                      <hr style={{margin:'0'}}/>
                      <button type='button' style={{border:'none',backgroundColor:'white',flexGrow:'1',outline:'none',fontFamily: 'Segoe UI,sans-serif'}} onClick={()=>{ 
                          if (session ==='open') {
                            terminal.proc.stdin.write(tinker.user_input+'\n') ;
                          }else{
                            alert('session closed');
                          }
                      }}>Send response </button>
                    </div>
                  </div>
                  }
                </div>
              
            </Modal>

        </div>
        { props.mode==="design" ? (
           <div style={{display:"flex", flexGrow:"1",backgroundColor:"#4a4a4a", fontFamily:"SegoeUI,sans-serif"}}
              onBlur={(e)=>{e.stopPropagation(); /*console.log('focus lost');*/ exportCodeFs(props.project, columns,'src'); }} 
              onFocus={(e)=>{e.stopPropagation(); /*console.log('focus'); */ refreshCode(props.project,columns,'src');}} tabIndex="0" >
                {
                 columns.map((column,i) => (
                    <div key={i} className="ussd-builder" style= {{ display:"flex", flexDirection:"column", margin:"2px", paddingRight:"1rem", paddingLeft:"1rem", justifyContent:"flex-start", borderRight:"1px solid gray"}}>
                      {column.opt.map((col,j)=> ( <Opt key={j} ci={i} oi={j} columns={columns} setColumns={setColumns} drg={drg} setDrag={setDrag} cap={codeWrapper} u_type={props.u_type}/>  )) }
                    </div>
                  )) 
                }
            </div>
          ) : (
        <div style={{display:"flex", flexGrow:"1",width:"100vw",height:"85vh"}}>
          <div style={{minWidth:"10rem", display:"flex", flexDirection:"Column", paddingLeft:"1rem"}}>
          <label style={{color:"white"}}> Project {props.project}</label>
          <hr style={{marginRight:"0",marginLeft:0, border:"none", borderTop:"1px solid white"}}/>
          {
            (()=>{
              let dirPath = rootDir+props.project+'/src';
              var files = fs.readdirSync(dirPath);
              
              return  files.map((file,i)=>{
                let isDir = fs.lstatSync(dirPath+'/'+file).isDirectory();   return ( <SideFile key={i}  file_name={file} dirPath={isDir?dirPath+"/"+file: dirPath} isDir={isDir} setDrag={setDrag} level={0} />  )} )
            })()
            } </div>
          
          
            <Ide backingFile={(coding.tab==='Hooks.js'||coding.tab===props.u_type+'.js')?rootDir+props.project+'/src/'+props.u_type+'/'+coding.tab : rootDir+props.project+'/src/'+coding.tab} coding={coding} setCoding={setCoding} updateParam={coding.tab.toLowerCase().substring(0,coding.tab.indexOf('.'))} u_type={props.u_type+'.js'}/>
            
          
        </div>)
      }
    </React.Fragment>
  }
  </React.Fragment>
  );
  

  
}
/* when porting this function to toher languages, rigorously test regex */
function buildPolyfill(columns, rootCode){
  const qc ='*737#'; /*user_inputs.substring(0,user_inputs.indexOf('#'));*/
  let pf = '\n\nconst buildPolyfill = (user_inputs,stages)=>{\n\tlet pf_inputs = [...user_inputs] \n'
  pf+= '\n\tconst r = /\\*[a-zA-Z_0-9]{1,}/g;'
  pf+= '\n\tlet diff=0'
  pf+= '\n\tlet prefix = "";'
  pf+= '\n\tlet firstQuery = pf_inputs[0];'
  pf+= '\n\tlet st = firstQuery.match(r).map(o=>o.replace("*",""));'
  pf+= '\n\tlet fa = []'
  pf+= '\n\tlet j = []'
  
  pf+='\n';
  columns.map((col,i)=>col.opt.map((pot,j)=>{
    if (pot.quickCode !==''){
          let pit = pot.quickCode.match(/\*{[a-z_][a-z_0-9]+}/g); //the array of stages included in the ussd code
          
          if(pit!==null && pit!==undefined){
              //This scenario is handled in the mirror function below. It is important this is handled after
              //because this deals with dynamic ussd quickcodes using inputs from other stages.
              // It is not static like *001*5# which deals with pin resets, but instaead
              // *001*amount#. it would be unnaceptable to *001*5# to trigger the code for *001*amount#.
              //thats why it is done this way.
          }else { 

            pf+= "\n\tj=[];\n\tif (user_inputs[0] === ('"+pot.quickCode+"') ) {\n"
            // this has a quick 
            // trace this branch to the beginning
            let pfArray = retrace(rootCode, pot,columns,i)
        
            pf+= "\t\tj  = ["+pfArray+"]\n";
            pf+="\t\tpf_inputs.splice(0,1,...j)"
            pf+='\n\t\tdiff = pf_inputs.length - user_inputs.length;'
            pf+='\n\t\treturn {"polyfill":pf_inputs, "diff":diff};\n\t}\n'
        }
      }
  }));
  columns.map((col,i)=>col.opt.map((pot,j)=>{
    if (pot.quickCode !==''){
          let pit = pot.quickCode.match(/\*{[a-z_][a-z_0-9]+}/g); //the array of stages included in the ussd code
          
          if(pit!==null && pit!==undefined){
            let key_array = pit.map(pi=>pi.substring(2,pi.length-1) );
            let pfArray = advancedRetrace(rootCode, pot,columns,i,key_array)
            
            pf+="\n\tfa = ["+pit.map(it => "'"+it+"'")+"];"
            pf+="\n\tj = ["+pfArray+"]";
            pf+="\n\tprefix = '"+pot.quickCode.split("{")[0]+"'"
            pf+="\n\tif (firstQuery.startsWith(prefix) && j.length === st.length){ "
            pf+="\n\t\tfor (i=0; i<j.length; i++){"
            pf+="\n\t\t\tfor (k=0; k<fa.length; k++){"
            pf+="\n\t\t\t\tlet z = fa[k].substring(2,fa[k].length-1);"
            pf+="\n\t\t\t\tif(z === j[i]){ // this means we have located a dynamic field"
            pf+="\n\t\t\t\t\tlet y = st[i]"
            pf+="\n\t\t\t\t\tstages[z] = {'input' :  y}"
            pf+="\n\t\t\t\t\tj[i] = y;"
            pf+="\n\t\t\t\t}"
            pf+="\n\t\t\t}"
            pf+="\n\t\t}"
            pf+="\n\t\tpf_inputs.splice(0,1,...j)"
            pf+='\n\t\tlet s = prefix.match(r).length-1'
            pf+='\n\t\tdiff = pf_inputs.length - user_inputs.length ;'
            pf+='\n\t\treturn {"polyfill":pf_inputs, "diff":diff};\n\t}\n'
          }
      }
  }))
  pf += '\n\treturn {"polyfill":pf_inputs, "diff":diff}; \n}';

  //console.log(pf);
  return pf;
}

const advancedRetrace = (rootCode, opt_c,columns,ci,key_array)=>{
    let pfText = '';
    let pfArray = [];
    while (ci!==0){
      //search through the key array first
      let pk = {'stage': '','stageIndex':0};
      opt_c.flowHeader.filter((fh,i)=>key_array.some(ka  =>{
        // using array.some method here to find the first match within the key_array. no need looping through the
        // whole key_array once a match is found.
        const ss = columns[fh.colPosition].opt[fh.optIndex].stageName;
        if ( ss=== ka) { pk={'stage':ka,stageIndex:i};    }  }));
      

      let header = opt_c.flowHeader[pk.stageIndex]; // defaultvalue of stageIndex is 0. this is good
      
      
      pfText =  '*'+(header.optTextIndex+1)+pfText; //optTextIndex starts from 0, but user inputs start from 1
      // if it's a stage, putin the stage. or else, put the textindex header
      pfArray.push(pk.stage!=='' ? "'"+pk.stage+"'" : "'"+(header.optTextIndex+1)+"'");
      ci = header.colPosition
      opt_c = columns[ci].opt[header.optIndex] // go back through any route.
    }
    pfText = rootCode.substring(0,rootCode.length-1)+pfText+"#"
    pfArray.push("'"+rootCode+"'");
    //console.log( "qc: "+opt_c.quickCode +" path : "+pfText)
    return pfArray.reverse();
}

const retrace = (rootCode, opt_c,columns,ci)=>{
    let pfText = '';
    let pfArray = [];
    while (ci!==0){
      let header = opt_c.flowHeader[0];
      pfText =  '*'+(header.optTextIndex+1)+pfText; //optTextIndex starts from 0, but user inputs start from 1
      pfArray.push("'"+(header.optTextIndex+1)+"'");
      ci = header.colPosition
      opt_c = columns[ci].opt[header.optIndex] // go back through any route.
    }
    pfText = rootCode.substring(0,rootCode.length-1)+pfText+"#"
    pfArray.push("'"+rootCode+"'");
    //console.log( "qc: "+opt_c.quickCode +" path : "+pfText)
    return pfArray.reverse();
}

function srcJs(depth, columns,trans_type,buildString, colIndex, optIndex, parentTabString){
    let tabString = "\t"+parentTabString ;
    let insert_pin = 'await terminalRequest(';
    let blocked_check = "let isUserBlocked = await blockedStatus(user_inputs , params ,'"
    
    let pref = 
`
const hook = require('./Hooks')
const inquirer = require('inquirer');
// sets the flow of the ussd session
async function handleRequest( phone_number,user_inputs,terminalRequest,blockedStatus,processPin,stages,all_inputs,polyfill_inputs,diff,params){
    if (polyfill_inputs.length === 0){
        let xy= buildPolyfill(user_inputs,stages)
        polyfill_inputs = xy.polyfill
        diff = xy.diff;
    }
    let len = polyfill_inputs.length;
    let err = { val:false , msg:'' , terminal:false }
  `;
      
    if (colIndex === 0){ // this is the root node
        buildString += pref;
        //recursively go through each opt in the column
      columns[colIndex].opt.forEach((op, opindex) =>{
        op.flows.forEach((fl, flindex)=>{
          depth = 1;
          var cltitle = colIndex > 0 ? columns[op.flowHeader[0].colPosition].opt[op.flowHeader[0].optIndex].flows[op.flowheader[0].optTextIndex].flowText :"root column" ;
          //alert("column"+colIndex+"  "+cltitle)
          //read the user's most recent input from the user_inputs array.
          // If this is the 1st flow in the opt, structure an if block, else structure an /else if block 

          trans_type= fl.flowText;
          let hookLine = "";
          if (op.stageName!=='' && op.flowType!=="pin") { hookLine = "stages['"+op.stageName+"'] = {'input' :  input}" } 
          let suff = flindex==0?"input = "+"polyfill_inputs["+depth+"]"+"\n"+tabString+hookLine+"\n"+tabString+"err = hook["+colIndex+"]["+optIndex+"](input,stages)"+"\n\n"+tabString+"if" : "else if";
          buildString +=  "\n"+tabString+suff+"( err.val === false && input!==undefined && input == '"+(flindex+1)+"'){"
          // When you get to an opt with child,  dive into that branch
          if( fl.flowChildIndex!==null ){ 
            //alert(flindex+"  "+fl.flowText)
            // diving into the flow branch here. flowChildIndex-1 is used here bacause flowChildIndex positions are numbered from 1 instead of 0  which the lnked array uses
            buildString = srcJs(depth,columns,trans_type,buildString,fl.flowChildColumn, fl.flowChildIndex,tabString)
          }else { 
            //When you get to an opt with no child, this is a terminal node, just add a closing curly brace ?. 
            /*alert(flindex+" terminal node "+fl.flowText); */ buildString +="\n"+tabString+"}" }
            if (flindex+1 === op.flows.length) {
              // We are in the last flow in the Opt, so we compile all the flows into a menu
              let ft = op.flows.map((f, fi)=> (fi+1)+" "+f.flowText); //- numbering in the mwnu starts from 1
              // prepare the response to be sent back to the user by joining the ft array
              let response = "\n\t"+tabString+"var response="+"\n"+tabString+"\t`"+mapInstructions(columns, op.ins)+"\n"+tabString+"\t"+ft.join( "\n\t"+tabString)+"`";
              // read the user's selection, and add it to the correct position in the user_inputs array.
              
              //let hookLine = "error = hook["+colIndex+"]["+optIndex+"](input,stages)";
              buildString += "\n"+tabString+"else{"
              buildString+= response
              buildString+= "\n\t"+tabString+"if ((params.index+1) === all_inputs.length){"
              buildString+=op.flowType!=="pin"?"\n\t\t"+tabString+"const answer = await inquirer.prompt([ { name: 'i',prefix:'"+freshText+"', message:response+'\\n'} ]);"+"\n\t\t"+tabString+"input = answer.i;"+"\n\t\t"+tabString+"user_inputs["+depth+"-diff]=input"+"\n\t\t"+tabString+"polyfill_inputs["+depth+"]=input"+"\n\t\t"+tabString+"all_inputs.push(input)"+"\n\t\t"+tabString+"params['index']=params.index+1" : ""
              buildString+="\n\t"+tabString+"}else{"
              buildString+="\n\t\t"+tabString+"user_inputs["+depth+"-diff]=all_inputs[params.index]"+"\n\t\t"+tabString+"polyfill_inputs["+depth+"]=all_inputs[params.index]"+"\n\t\t"+tabString+"input = all_inputs[params.index]"+"\n\t\t"+tabString+"params['index']=params.index+1"+"\n\t"+tabString+"}"
            //if (op.stageName!=='' && op.flowType!=="pin") {buildString += "\n\t"+tabString+"stages['"+op.stageName+"'] = {'input' :  input}"}
              buildString+="\n\t"+tabString+ "if (!params.exit && !(err.val && err.terminal) ) {";
              buildString += "\n\t\t"+tabString+"handleRequest( phone_number,user_inputs,terminalRequest,blockedStatus,processPin,stages,all_inputs,polyfill_inputs,diff,params)"+"\n\t\t"+tabString+"}"+"\n\t"+tabString+"else {"+"\n\t\t"+tabString+"response = !(err.val && err.terminal) ? `User cannot proceed past this point` : err.msg"+"\n\t\t"+tabString+"console.log(`"+freshText+"`+ response)"+"\n\t"+tabString+"}"
              buildString+="\n"+tabString+"}\n"
            } /*  this is an invalid input, reshow the menu */
        })
          
      });
      buildString+="\n} \n \n module.exports = handleRequest";
    }else { //We're not in the root node anymore
      depth ++;  // another level of recursion;
      let blockedLoop = "( isUserBlocked){\n\t"+tabString+'console.log("'+freshText+'"+params.blockedText);'+'\n\t'+tabString+'params["exit"]=true'+'\n\t'+tabString+'return;'+'\n'+tabString+"}\n"+tabString+"let pinResult = await processPin( phone_number, polyfill_inputs,"+depth+", user_inputs ,"+depth+"-diff, all_inputs,params ); "
      blockedLoop+="\n"+tabString+'if(pinResult === true){';
      var op = columns[colIndex].opt[optIndex]; 
      var cltitle = colIndex > 0 ? columns[op.flowHeader[0].colPosition].opt[op.flowHeader[0].optIndex].flows[op.flowHeader[0].optTextIndex].flowText :"root column" ;
      //recursively go through each opt in the column
      // //freshtext in the lower line of code is used as a delimeter from my phone emulator software
      if (op.flowType === "end")   {
        buildString+='\n'+tabString+'if ('+insert_pin+op.pinSetinPrevStage+",'"+trans_type+"',"+op.amountInputStage+",phone_number, polyfill_inputs,"+depth+", user_inputs ,"+depth+"-diff, all_inputs,params) === true){"
        buildString+= '\n\t'+tabString+'err = hook['+colIndex+']['+optIndex+']("",stages)'
        buildString+='\n\t'+tabString+'if( err.val === false){'
        buildString +="\n\t\t"+tabString+ "response =`"+mapInstructions(columns, op.ins)+"`;"+"\n\t"+tabString+"}"
        buildString+="\n\t"+tabString+"else{"+'\n\t\t'+tabString+"response = `${err.msg}`"+"\n\t"+tabString+"}"+'\n\t'+tabString+"console.log(`"+freshText+"`+ response)"; 
        buildString+='\n'+tabString+'}'+'\n'+parentTabString+"}"
            }
      
      op.flows.forEach((fl, flindex)=>{
        //read the user's most recent input from the user_inputs array.
        // If this is the 1st flow in the opt, structure an if block, else structure an /else if block 
        let hookLine = "";
        if (op.stageName!=='' && op.flowType!=="pin") { hookLine = "stages['"+op.stageName+"'] = {'input' :  input}" } 
          
        let suff = flindex==0?"input = "+"polyfill_inputs["+depth+"]"+"\n"+tabString+hookLine+"\n"+tabString+"err = hook["+colIndex+"]["+optIndex+"](input,stages)"+"\n\n"+tabString+"if" : "else if";
        //console.log(op);
        let errCheck = "  err.val === false "
        if (op.flowType === "menu" ){ 
          buildString +="\n"+tabString+suff+"("+errCheck+"&& input!==undefined && input == '"+(flindex+1)+"'){"; 
          if (!blessedList(trans_type)  && op.amountInputStage === 0 && op.textInputStage ===0 && !op.pinSetinPrevStage){ trans_type  = fl.flowText }
        }
        else if (op.flowType === "text" ){ buildString +="\n"+tabString+suff+"("+errCheck+"&& input!==undefined && input !== ''){"; }
        else if (op.flowType === "pin" ) {buildString +="\n"+tabString+blocked_check+(trans_type)+"',"+op.amountInputStage+")\n"+tabString+suff+blockedLoop; }
        else if (op.flowType === "amount" ){buildString +="\n"+tabString+suff+"("+errCheck+"&& input!==undefined && !isNaN(parseInt(input) ) && isFinite(input) &&  ("; 
            if (fl.flowRangeStart ==="" && fl.flowRangeStop ==="") { buildString += "parseInt(input) > "+0 }
            if (fl.flowRangeStart !== ""){ buildString += "parseInt(input) >= "+parseInt(fl.flowRangeStart)  }
            if (fl.flowRangeStop !== ""){ 
                buildString += fl.flowRangeStart !== "" ?" && " : "";  // input is changed depending on if the user specified a starting point
                buildString+= "parseInt(input) <= "+parseInt(fl.flowRangeStop) 
            }
          buildString +=")  ){";
        }
        //console.log("block done") ;
        //console.log("transaction_type "+trans_type)
    
        if(fl.flowChildIndex!==null){ // these flows are parent of opts in other columns
          /*alert(flindex+"  "+fl.flowText)*/
          // diving into the flow branch here. flowChildIndex-1 is used here bacause flowChildIndex positions are numbered from 1 instead of 0  which the lnked array uses
          buildString = srcJs(depth,columns,trans_type,buildString,fl.flowChildColumn, fl.flowChildIndex, tabString)
        }else { //these flows are not parents of opts in other columns
          /*alert(flindex+" terminal node "+fl.flowText); */
          buildString += op.flowType!=="pin" ? "\n\t"+tabString+"//"+flindex+" terminal node "+fl.flowText+"\n\t"+tabString+insert_pin+op.pinSetinPrevStage+",'"+trans_type+"',"+op.amountInputStage+",phone_number, polyfill_inputs,"+depth+", user_inputs ,"+depth+"-diff, all_inputs,params)" :""
          buildString +="\n"+tabString+"}" 
        }
        if (flindex+1 === op.flows.length) {
            let ft = op.flows.map((f, fi)=> (fi+1)+" "+f.flowText); 
            let response = "\n\t"+tabString+"var response=\n"+tabString+"\t`";
            response +=op.flowType!=="pin"?mapInstructions(columns, op.ins): "the flow ends here for failed pins";
            response += op.flowType !== "menu" ? "`" : "\n\t"+tabString+ft.join( "\n\t"+tabString)+"`" /* For handling different flow types */
            //response += op.flowType!=="pin"? "\n\t"+tabString+"const answer = await inquirer.prompt([ { name: 'i',prefix:'"+freshText+"', message:response+'\\n'} ]);"+"\n\t"+tabString+"input = answer.i;"+"\n\t"+tabString+"user_inputs["+depth+"-diff]=input":"" // for fetching user input
              
            buildString += "\n"+tabString+"else{"
            buildString+= response
            buildString+=(op.flowType!=="pin" && op.stageName!="")?'\n\t\t'+tabString+'let ply = params["retry"].some(v=> v==="'+op.stageName+'")'+'\n\t\t'+tabString+'if (diff > 0 && !ply && polyfill_inputs.length >'+depth+'){'+'\n\t\t\t'+tabString+'diff = '+depth+'- user_inputs.length'+'\n\t\t\t'+tabString+'params["retry"].push("'+op.stageName+'")'+'\n\t\t\t'+tabString+'polyfill_inputs.splice('+(depth+1)+')'+'\n\t\t'+tabString+'}' : ""
            buildString+= "\n\t"+tabString+"if ((params.index+1) === all_inputs.length){"
            buildString+=op.flowType!=="pin"?"\n\t\t"+tabString+"const answer = await inquirer.prompt([ { name: 'i',prefix:'"+freshText+"', message:response+'\\n'} ]);"+"\n\t\t"+tabString+"input = answer.i;"+"\n\t\t"+tabString+"user_inputs["+depth+"-diff]=input"+"\n\t\t"+tabString+"polyfill_inputs["+depth+"]=input"+"\n\t\t"+tabString+"all_inputs.push(input)"+"\n\t\t"+tabString+"params['index']=params.index+1" : ""
            buildString+="\n\t"+tabString+"}else{"
            buildString+=op.flowType!=="pin"?"\n\t\t"+tabString+"user_inputs["+depth+"-diff]=all_inputs[params.index]"+"\n\t\t"+tabString+"polyfill_inputs["+depth+"]=all_inputs[params.index]"+"\n\t\t"+tabString+"input = all_inputs[params.index]"+"\n\t\t"+tabString+"params['index'] = params.index+1":""
            buildString+="\n\t"+tabString+"}"
            buildString+=op.flowType === "pin" ? "\n\t"+tabString+'params["exit"] = true;' : "" // exits the app in case of dfailed pin scenario, since user is limited to one pin per session
            //if (op.stageName!=='' && op.flowType!=="pin") {buildString += "\n\t"+tabString+"stages['"+op.stageName+"'] = {'input' :  input}"}
            // handleRequest should not be called in case of PIN Authentication Failure, this prevents the user from retrying after fdailed pins           
            //let hookLine = "let error = hook["+colIndex+"]["+optIndex+"](input,stages)";
            //buildString += "\n\t"+tabString+hookLine
            buildString += "\n\t"+tabString+"if (!params.exit && !(err.val && err.terminal) ) {"+"\n\t\t"+tabString+"handleRequest( phone_number, user_inputs,terminalRequest,blockedStatus,processPin,stages,all_inputs,polyfill_inputs,diff,params)"+"\n\t"+tabString+"}"+"\n\t"+tabString+"else {"+"\n\t\t"+tabString+"response = !(err.val && err.terminal) ? `User cannot proceed past this point` : err.msg"+"\n\t\t"+tabString+"console.log(`"+freshText+"`+response)"+"\n\t"+tabString+"}"; 
            buildString+="\n"+tabString+"}\n"+parentTabString+"}" ; /*  this is an invalid input, reshow the menu */
        }
      })
    }
    return buildString;
}


function buildJs(depth, columns,trans_type,buildString, colIndex, optIndex, parentTabString){
    let tabString = "\t"+parentTabString ;
    let insert_pin = 'let terminalPin = await terminalRequest(';
    let blocked_check = "let isUserBlocked = await blockedStatus( user_inputs, params,'"
    
    let pref = 
`
const hook = require('./Hooks')
const inquirer = require('inquirer');


// sets the flow of the ussd session
async function handleRequest( phone_number,user_inputs,terminalRequest,blockedStatus,processPin,stages,all_inputs,polyfill_inputs,diff,params){
    if (polyfill_inputs.length === 0){
        let xy= buildPolyfill(user_inputs,stages)
        polyfill_inputs = xy.polyfill
        diff = xy.diff;
    }
    let len = polyfill_inputs.length;
    let err = { val:false , msg:'' , terminal:false }
  `;
      
    if (colIndex === 0){ // this is the root node
        buildString += pref;
        //recursively go through each opt in the column
      columns[colIndex].opt.forEach((op, opindex) =>{
        op.flows.forEach((fl, flindex)=>{
          depth = 1;
          var cltitle = colIndex > 0 ? columns[op.flowHeader[0].colPosition].opt[op.flowHeader[0].optIndex].flows[op.flowheader[0].optTextIndex].flowText :"root column" ;
          //alert("column"+colIndex+"  "+cltitle)
          //read the user's most recent input from the user_inputs array.
          // If this is the 1st flow in the opt, structure an if block, else structure an /else if block 

          trans_type= fl.flowText;
          let hookLine = "";
          if (op.stageName!=='' && op.flowType!=="pin") { hookLine = "stages['"+op.stageName+"'] = {'input' :  input}" } 
          let suff = flindex==0?"input = "+"polyfill_inputs["+depth+"]"+"\n"+tabString+hookLine+"\n"+tabString+"err = hook["+colIndex+"]["+optIndex+"](input,stages)"+"\n\n"+tabString+"if" : "else if";
          buildString +=  "\n"+tabString+suff+"( err.val === false && input!==undefined && input == '"+(flindex+1)+"'){"
          // When you get to an opt with child,  dive into that branch
          if( fl.flowChildIndex!==null ){ 
            //alert(flindex+"  "+fl.flowText)
            // diving into the flow branch here. flowChildIndex-1 is used here bacause flowChildIndex positions are numbered from 1 instead of 0  which the lnked array uses
            buildString = buildJs(depth,columns,trans_type,buildString,fl.flowChildColumn, fl.flowChildIndex,tabString)
          }else { 
            //When you get to an opt with no child, this is a terminal node, just add a closing curly brace ?. 
            /*alert(flindex+" terminal node "+fl.flowText); */ buildString +="\n"+tabString+"}" }
            if (flindex+1 === op.flows.length) {
              // We are in the last flow in the Opt, so we compile all the flows into a menu
              let ft = op.flows.map((f, fi)=> (fi+1)+" "+f.flowText); //- numbering in the mwnu starts from 1
              // prepare the response to be sent back to the user by joining the ft array
              let response = "\n\t"+tabString+"var response="+"\n"+tabString+"\t`"+mapInstructions(columns, op.ins)+"\n"+tabString+"\t"+ft.join( "\n\t"+tabString)+"`";
              // read the user's selection, and add it to the correct position in the user_inputs array.
              
              //let hookLine = "error = hook["+colIndex+"]["+optIndex+"](input,stages)";
              buildString += "\n"+tabString+"else{"
              buildString+= response
              buildString+= "\n\t"+tabString+"if ((params.index+1) === all_inputs.length){"
              buildString+=op.flowType!=="pin"?"\n\t\t"+tabString+"user_inputs["+depth+"-diff]=input"+"\n\t\t"+tabString+"polyfill_inputs["+depth+"]=input"+"\n\t\t"+tabString+"all_inputs.push(input)"+"\n\t\t"+tabString+"params['index']=params.index+1" : ""
              buildString+="\n\t"+tabString+"}else{"
              buildString+="\n\t\t"+tabString+"user_inputs["+depth+"-diff]=all_inputs[params.index]"+"\n\t\t"+tabString+"polyfill_inputs["+depth+"]=all_inputs[params.index]"+"\n\t\t"+tabString+"input = all_inputs[params.index]"+"\n\t\t"+tabString+"params['index']=params.index+1"+"\n\t"+tabString+"}"
            //if (op.stageName!=='' && op.flowType!=="pin") {buildString += "\n\t"+tabString+"stages['"+op.stageName+"'] = {'input' :  input}"}
              buildString+="\n\t"+tabString+ "if (!params.exit && !(err.val && err.terminal) ) {";
              buildString += "\n\t\t"+tabString+"return response;"+"\n\t\t"+tabString+"}"+"\n\t"+tabString+"else {"+"\n\t\t"+tabString+"response = !(err.val && err.terminal) ? `User cannot proceed past this point` : err.msg"+"\n\t\t"+tabString+"return response"+"\n\t"+tabString+"}"
              buildString+="\n"+tabString+"}\n"
            } /*  this is an invalid input, reshow the menu */
        })
          
      });
      buildString+="\n} \n \n module.exports = handleRequest";
    }else { //We're not in the root node anymore
      depth ++;  // another level of recursion;
      let blockedLoop = '( isUserBlocked.status)return "blocked";'+"\n\n"+tabString+"let pinResult = await processPin( phone_number, polyfill_inputs,"+depth+", user_inputs ,"+depth+"-diff, all_inputs,params ); "
      blockedLoop+= "\n"+tabString+'if (pinResult === null){'+"\n\t"+tabString+'return "Please input your PIN: "'+'\n'+tabString+'}';
      blockedLoop+="\n"+tabString+'else if(pinResult === true){'
      
      let blockedCircuit = '\n'+tabString+'if (isUserBlocked.reqError !== null) return "Invalid Session";'
      let terminalCircuit = '\n'+tabString+'if (terminalPin.reqError !== null) return "Invalid Session";'
      
      var op = columns[colIndex].opt[optIndex]; 
      var cltitle = colIndex > 0 ? columns[op.flowHeader[0].colPosition].opt[op.flowHeader[0].optIndex].flows[op.flowHeader[0].optTextIndex].flowText :"root column" ;
      //recursively go through each opt in the column
      // //freshtext in the lower line of code is used as a delimeter from my phone emulator software
      if (op.flowType === "end")   {
        buildString += '\n'+tabString+insert_pin+op.pinSetinPrevStage+",'"+trans_type+"',"+op.amountInputStage+",phone_number, polyfill_inputs,"+depth+", user_inputs ,"+depth+"-diff, all_inputs,params)"
        buildString+=terminalCircuit
        buildString+='\n'+tabString+'if (terminalPin.status === null){'+"\n\t"+tabString+'return "Please input your PIN :"'+"\n"+tabString+"}"
        buildString+='\n'+tabString+'else if (terminalPin.status === false){'+'\n\t'+tabString+' return "PIN is incorrect"'+'\n'+tabString+'}'
        buildString+='\n'+tabString+'else if (terminalPin.status === true){'
        buildString+= '\n\t'+tabString+'err = hook['+colIndex+']['+optIndex+']("",stages)'
        buildString+='\n\t'+tabString+'if( err.val === false){'
        buildString +="\n\t\t"+tabString+ "response =`"+mapInstructions(columns, op.ins)+"`;"+"\n\t"+tabString+"}"
        buildString+="\n\t"+tabString+"else{"+'\n\t\t'+tabString+"response = `${err.msg}`"+"\n\t"+tabString+"}"+'\n\t'+tabString+"return  response"; 
        buildString+='\n'+tabString+'}'+'\n'+parentTabString+"}"
            }
      
      op.flows.forEach((fl, flindex)=>{
        //read the user's most recent input from the user_inputs array.
        // If this is the 1st flow in the opt, structure an if block, else structure an /else if block 
        let hookLine = "";
        if (op.stageName!=='' && op.flowType!=="pin") { hookLine = "stages['"+op.stageName+"'] = {'input' :  input}" } 
          
        let suff = flindex==0?"input = "+"polyfill_inputs["+depth+"]"+"\n"+tabString+hookLine+"\n"+tabString+"err = hook["+colIndex+"]["+optIndex+"](input,stages)"+"\n\n"+tabString+"if" : "else if";
        //console.log(op);
        let errCheck = "  err.val === false "
        if (op.flowType === "menu" ){ 
          buildString +="\n"+tabString+suff+"("+errCheck+"&& input!==undefined && input == '"+(flindex+1)+"'){"; 
          if ( !blessedList(trans_type) && op.amountInputStage === 0 && op.textInputStage ===0 && !op.pinSetinPrevStage){ trans_type  = fl.flowText }
        }
        else if (op.flowType === "text" ){ buildString +="\n"+tabString+suff+"("+errCheck+"&& input!==undefined && input !== ''){"; }
        else if (op.flowType === "pin" ) {buildString +="\n"+tabString+blocked_check+(trans_type)+"',"+op.amountInputStage+")\n"+tabString+blockedCircuit+"\n"+tabString+suff+blockedLoop; }
        else if (op.flowType === "amount" ){buildString +="\n"+tabString+suff+"("+errCheck+"&& input!==undefined && !isNaN(parseInt(input) ) && isFinite(input) && ("; 
            if (fl.flowRangeStart ==="" && fl.flowRangeStop ==="") { buildString += "parseInt(input) > "+0 }
            if (fl.flowRangeStart !== ""){ buildString += "parseInt(input) >= "+parseInt(fl.flowRangeStart)  }
            if (fl.flowRangeStop !== ""){ 
                buildString += fl.flowRangeStart !== "" ?" && " : "";  // input is changed depending on if the user specified a starting point
                buildString+= "parseInt(input) <= "+parseInt(fl.flowRangeStop) 
            }
          buildString +=")  ){";
        }
        //console.log("block done") ;
        //console.log("transaction_type "+trans_type)
    
        if(fl.flowChildIndex!==null){ // these flows are parent of opts in other columns
          /*alert(flindex+"  "+fl.flowText)*/
          // diving into the flow branch here. flowChildIndex-1 is used here bacause flowChildIndex positions are numbered from 1 instead of 0  which the lnked array uses
          buildString = buildJs(depth,columns,trans_type,buildString,fl.flowChildColumn, fl.flowChildIndex, tabString)
        }else { //these flows are not parents of opts in other columns
          /*alert(flindex+" terminal node "+fl.flowText); */
          buildString += op.flowType!=="pin" ? "\n\t"+tabString+"//"+flindex+" terminal node "+fl.flowText+"\n\t"+tabString+insert_pin+op.pinSetinPrevStage+",'"+trans_type+"',"+op.amountInputStage+",phone_number, polyfill_inputs,"+depth+", user_inputs ,"+depth+"-diff, all_inputs,params)" :""
          buildString+= op.flowType!=="pin" ?   terminalCircuit+'\n\t'+tabString+'if (terminalPin.status=== null){'+'\n\t\t'+tabString+'return "Please input your PIN :"'+"\n\t"+tabString+"}" : ""
          buildString+= op.flowType!=="pin" ? '\n\t'+tabString+'else if (terminalPin.status === false){'+'\n\t\t'+tabString+' return "PIN is incorrect"'+'\n\t'+tabString+'}' : ""
          
          buildString +="\n"+tabString+"}" 
        }
        if (flindex+1 === op.flows.length) {
            let ft = op.flows.map((f, fi)=> (fi+1)+" "+f.flowText); 
            let response = "\n\t"+tabString+"var response=\n"+tabString+"\t`";
            response +=op.flowType!=="pin"?mapInstructions(columns, op.ins): "the flow ends here for failed pins";
            response += op.flowType !== "menu" ? "`" : "\n\t"+tabString+ft.join( "\n\t"+tabString)+"`" /* For handling different flow types */
            //response += op.flowType!=="pin"? "\n\t"+tabString+"const answer = await inquirer.prompt([ { name: 'i',prefix:'"+freshText+"', message:response+'\\n'} ]);"+"\n\t"+tabString+"input = answer.i;"+"\n\t"+tabString+"user_inputs["+depth+"-diff]=input":"" // for fetching user input
              
            buildString += "\n"+tabString+"else{"
            buildString+= response
            buildString+=(op.flowType!=="pin" && op.stageName!="")?'\n\t\t'+tabString+'let ply = params["retry"].some(v=> v==="'+op.stageName+'")'+'\n\t\t'+tabString+'if (diff > 0 && !ply && polyfill_inputs.length >'+depth+'){'+'\n\t\t\t'+tabString+'diff = '+depth+'- user_inputs.length'+'\n\t\t\t'+tabString+'params["retry"].push("'+op.stageName+'")'+'\n\t\t\t'+tabString+'polyfill_inputs.splice('+(depth+1)+')'+'\n\t\t'+tabString+'}' : ""
            buildString+= "\n\t"+tabString+"if ((params.index+1) === all_inputs.length){"
            buildString+=op.flowType!=="pin"?"\n\t\t"+tabString+"user_inputs["+depth+"-diff]=input"+"\n\t\t"+tabString+"polyfill_inputs["+depth+"]=input"+"\n\t\t"+tabString+"all_inputs.push(input)"+"\n\t\t"+tabString+"params['index']=params.index+1" : ""
            buildString+="\n\t"+tabString+"}else{"
            buildString+=op.flowType!=="pin"?"\n\t\t"+tabString+"user_inputs["+depth+"-diff]=all_inputs[params.index]"+"\n\t\t"+tabString+"polyfill_inputs["+depth+"]=all_inputs[params.index]"+"\n\t\t"+tabString+"input = all_inputs[params.index]"+"\n\t\t"+tabString+"params['index'] = params.index+1":""
            buildString+="\n\t"+tabString+"}"
            buildString+=op.flowType === "pin" ? "\n\t"+tabString+'params["exit"] = true;' : "" // exits the app in case of dfailed pin scenario, since user is limited to one pin per session
            //if (op.stageName!=='' && op.flowType!=="pin") {buildString += "\n\t"+tabString+"stages['"+op.stageName+"'] = {'input' :  input}"}
            // handleRequest should not be called in case of PIN Authentication Failure, this prevents the user from retrying after fdailed pins           
            //let hookLine = "let error = hook["+colIndex+"]["+optIndex+"](input,stages)";
            //buildString += "\n\t"+tabString+hookLine
            buildString += "\n\t"+tabString+"if (!params.exit && !(err.val && err.terminal) ) {"+"\n\t\t"+tabString+"return response;"+"\n\t"+tabString+"}"+"\n\t"+tabString+"else {"+"\n\t\t"+tabString+"response = !(err.val && err.terminal) ? `User cannot proceed past this point` : err.msg"+"\n\t\t"+tabString+"return  response"+"\n\t"+tabString+"}"; 
            buildString+="\n"+tabString+"}\n"+parentTabString+"}" ; /*  this is an invalid input, reshow the menu */
        }
      })
    }
    return buildString;
}

function mapInstructions(columns, ins){
  let t = ins.match(/{*[a-z_][a-z_0-9]+\.input}/g)
  //console.log(t)
  if(t!==null && t!==undefined){
    t.forEach((t_obj,i) =>{
      columns.map((col,c)=>{
        col.opt.map((op,o)=>{
          if ('{'+op.stageName+'.input}' === t_obj){
            //console.log('found '+t_obj)
            //the two string are equal. , meaning that the stage does exist
            ins = ins.replace(t_obj, '${stages.'+op.stageName+'.input}')
          }
        })
      })
    })
  }//console.log(ins)
  return ins;

}

/*do not write to fs in this method, just return the text, and let the calling method do as it will */
function buildOverride(projectPath) {
  if (fs.existsSync(projectPath+'/src/Override.js')){
  var contents = fs.readFileSync(projectPath+'/src/Override.js', 'utf8');
  return contents;
}
  const text = 
`
// You can Re-implement the method below to your taste

const processPin = async function ( phoneNumber, pin){
    const PIN = "1234";
    if  ( pin === PIN){
      return true;
    }
    return false;
}


//This method determines if a user_id is using this service for the first time

const isFirstTimeUser = async function (phoneNumber ){
  // return a value indicating if this is a 1st time user or not

  return false;
}

const saveRequest = async function (SESSION_ID , phoneNumber , request_key){
  
  //save the request into  a cache/db of your chosing
  //it is recommended you use the SESSION_ID as key when designing your cache/db

}

// Returns request of a user from your cache/db OR null if the request_key does not exist
const getRequest = async function(SESSION_ID , phoneNumber){


}


module.exports = {
  processPin : processPin,
  isFirstTimeUser : isFirstTimeUser,
  saveRequest : saveRequest,
  getRequest : getRequest,
  blockedText : "Reset your PIN to unblock your account"
}
`

  return text ;
}
function getTransactionType(columns,fl , currentOpt, colIndex){
  /*
  var amt_stage = currentOpt.amountInputStage;
  var txt_stage = currentOpt.textInputStage;
  if (amt_stage ===0 && txt_stage===0){
    return fl.flowText;
  }
  else {
    var r_branch = relevantBranch(amt_stage, txt_stage)
    //alert(JSON.stringify(currentOpt))
    var prevOpt = reverse(columns,currentOpt,colIndex-1,r_branch);
    var flowIndex = prevOpt.flowHeader.optTextIndex
    prevOpt = columns[r_branch-1].opt[prevOpt.flowHeader.optIndex];
    while (prevOpt.flowType !== "menu"){
      r_branch--;
      flowIndex = prevOpt.flowHeader.optTextIndex
      prevOpt = reverse(columns,prevOpt,r_branch,r_branch-1)
    }
    var prevFlow = prevOpt.flows[flowIndex]
    //alert("transaction_type "+prevFlow.flowText)
    return prevFlow.flowText
  }
  */
  
  return ""
}
function rwnd(columns, cOpt, colIndex){
  var prevOpt = columns[colIndex].opt[cOpt.flowHeader.optIndex];
  return prevOpt
}
function reverse (columns,currentOpt, startIndex, stopIndex){
  let cOpt = currentOpt;
  for (let i=startIndex; i>=stopIndex; i--){
    cOpt = rwnd(columns,cOpt, i)
    //alert(i+" "+JSON.stringify(cOpt) )
  }
  return cOpt;
}
function relevantBranch(a, b) {
  if (a===0 || b===0) return Math.max(a,b) 
  else 
    return Math.min(a,b);
}

/* checks if the transaction type is part of the blessed List */
function blessedList(trans_type){
    let bl = ["pin_reset","pin_change"]

    return bl.some(e=> trans_type === e)

}

//* the text that should be in our root skeleton file stored in fs */
function skeletonSrc(rootCode){
const s =   
`// DO NOT EDIT THIS DOCUMENT
// Generated ussd code by ptrikey SADE Framework
// This code has been logged to your console, copy it and paste it in a .js file - save the file with name of your choice
// To run this, node.js must be installed on your workstation, this is the format for running it below
// navigate to the directory where Skeleton.js is saved and run the command below
// node Skeleton.js [phone_number_making_the_ussd_request] [account_no_tied_to_this_phone_number] [api_key] [imei]
// here is a valid api_key 100000000

const axios = require('axios');
const inquirer = require('inquirer');
const overrides = require('./Override')
const normalUserResponse = require('./normal_user/normal_user')
const firstTimeResponse = require('./first_time_user/first_time_user')
const args = require('yargs').argv;

const STATUS_VERIFY = "verify";
const STATUS_BLOCKED = "blocked"
const STATUS_OK = "ok"
const REQUEST_ABNORMAL ="abnormal"
const REQUEST_NORMAL ="normal"
const OUTCOME_SUCCESS = "success"
const OUTCOME_FAILURE = "failure"
const PIN_CORRECT = "correct"
const PIN_WRONG = "wrong"
const PIN_RETRY_LIMIT = 1 //recomended value of 1
const FIRST_TIME_USER ="first_time_user";
const NORMAL_USER = "normal_user"
const PORT = "8000";

/* fill in these flags */
let phone_number = args.ph;
let user_id =args.id;
let api_key = args.key;
let imei = args.imei;
let ussd=args.ussd;

let user_inputs=[] // initially filled with your ussd root code
let stages = {} // the ussd stages object
let request_key =""
let input = "";
let pinasked = "no";
let exit = false;

fillInput();
sadeInit();

async function sadeInit(){
  if (await overrides.isFirstTimeUser(phone_number)){
    firstTimeResponse(phone_number,user_inputs,terminalRequest,blockedStatus,processPin,stages,[ussd],[],0,{"index":0,"retry":[],"request_key":null, "userType":FIRST_TIME_USER, exit:false, "blockedText":overrides.blockedText}) 
  }
  else{
    checkUserStatus();  
  }
}

 async function sendSummary(outcome,pin_entries,user_inputs ){
    const {data: summary_data} = await axios.post("https://ptrikey.com:"+PORT+"/demo/summary",  {req:request_key, pin_asked: pinasked ,pin_status:pin_entries, 
      session_inputs:user_inputs, transaction_outcome:outcome==true? OUTCOME_SUCCESS : OUTCOME_FAILURE})
    console.log(summary_data.remark) 
}

async function checkUserStatus(){
    console.log("checking status")
    const {data:safety_data} = await axios.post("https://ptrikey.com:"+PORT+"/demo/safety", { phone: phone_number , user_id: user_id ,  api_key:api_key, imei: imei })
    request_key = safety_data.req;
    
    if (safety_data.status===STATUS_VERIFY){
        const answer = await inquirer.prompt([ { name: "i",prefix:"${freshText}", message:safety_data.sec_question+"\\n" } ]);
        let ans = answer.i;

        const {data:response_data} = await axios.post("https://ptrikey.com:"+PORT+"/demo/validate", {req:request_key,answer: ans});
    }else if (safety_data.status === STATUS_BLOCKED){
      
        let resp = await normalUserResponse(phone_number,user_inputs,terminalRequest,blockedStatus,processPin,stages,[ussd],[],0,{"index":0,"retry":[], "request_key":request_key,"exit":false , "userType":NORMAL_USER,"blockedText":overrides.blockedText}); 
        if (resp === STATUS_BLOCKED ){
            sendSummary(false,[],request_key,user_inputs );
            console.log( "${freshText}"+overrides.blockedText );
        }

    }else if (safety_data.status === STATUS_OK){
        normalUserResponse(phone_number,user_inputs,terminalRequest,blockedStatus,processPin,stages,[ussd],[],0,{"index":0,"retry":[] , "request_key":request_key, exit:false ,"userType":NORMAL_USER, "blockedText":overrides.blockedText}); 
    }
}

async function terminalRequest(pin_set_in_prev_stage, transaction_type, amount_stage,phone_number, polyfill_inputs, pfindex, user_inputs , user_index , all_inputs,params){
    // this is called before the terminal stage of a transaction, to indicate if a pin should be asked or not
    let outcome = true;
    
    if (params.userType !== FIRST_TIME_USER){
        amount_stage += 1;
        let pin_entries = [];
        let amt = 0;
        if (amount_stage>1 && polyfill_inputs.length> amount_stage){
            amt = polyfill_inputs[amount_stage];
        }
        if (pin_set_in_prev_stage===false){ 
            const {data: request_data} = await axios.post("https://ptrikey.com:"+PORT+"/demo/request",  {req :request_key, pin_required:"no",currency:"NGN", 
            transaction_type: transaction_type ,request_inputs_array: user_inputs, amount: amt*100});
            if (request_data.remark === REQUEST_ABNORMAL){// embed pin checking stage
                outcome =await processPin ( phone_number, polyfill_inputs, pfindex, user_inputs , user_index , all_inputs,params)
                pin_entries = [outcome ? PIN_CORRECT : PIN_WRONG]
            }else if (request_data.remark === REQUEST_NORMAL) {
                // complete the transaction, then store the outcome in a variable

            } 
        }else{
            user_inputs.map(how=>{
                if(how === PIN_CORRECT){
                    pin_entries.push(PIN_CORRECT)
                }
                if(how === PIN_WRONG){
                    user_inputs.push(PIN_WRONG)
                }

            })
        }

        await sendSummary(outcome,pin_entries,user_inputs); // finally, do this
    }
    return outcome; // true or false 
}


async function blockedStatus(user_inputs , params ,transaction_type, amount_stage){
  // this is called before the terminal stage of a transaction, to indicate if a pin should be asked or not
    amount_stage += 1;
    let amt = 0;
    if (amount_stage > 1 && user_inputs.length> amount_stage){
        amt = user_inputs[amount_stage];
    }
    const {data: request_data} = await axios.post("https://ptrikey.com:"+PORT+"/demo/request",  {req :request_key, pin_required:"yes",currency:"NGN", 
      transaction_type: transaction_type ,request_inputs_array: user_inputs ,amount: amt*100});
    if (request_data.remark === STATUS_BLOCKED){// embed pin checking stage
        return true;
    }
    return false ;
}

async function unblock(){
    console.log("${freshText}Reset your PIN to unblock your account")
}

async function processPin( phone_number, polyfill_inputs, pfindex, user_inputs , user_index , all_inputs,params){
    // make all the api_call you need 
    pinasked="yes";
    let pin = '';
    let pin_entries = [];
    if (polyfill_inputs[pfindex] === undefined){ 
        /* the pin has not been asked before. this is okay because for now, we're limiting
        the ussd apps to one pin entry per session */
        const answer = await inquirer.prompt([ { name: 'i',prefix:'<ussd-ussd>', message:"Please input your pin:\\n" } ]);
        pin = answer.i;
        all_inputs.push(pin);
        params['index'] = params.index+1;
        
        polyfill_inputs[pfindex] = pin;
    }else {
        pin = polyfill_inputs[pfindex]
    }
    
    isPinCorrect = await overrides.processPin( phone_number, pin );
       
    if  ( isPinCorrect){
        user_inputs[user_index] = PIN_CORRECT;
        console.log("${freshText}Authentication Success")
        pin_entries.push(PIN_CORRECT);
        return true;
    }
    console.log("${freshText}Authentication Failure")
    pin_entries.push(PIN_WRONG);
    user_inputs[user_index] = PIN_WRONG;
    if (pin_entries.length >= PIN_RETRY_LIMIT ){ 
      await sendSummary(false,pin_entries,user_inputs);  
      params["exit"]=true;
    }
  
    return false;
}


function fillInput(){

    while(ussd.includes('**')) {
        ussd = ussd.replace('**','*')
    }
    while(ussd.includes('##')) {
        ussd = ussd.replace('##','#')
    }
    let uv = ussd.split('#');
    for (i=0; i<uv.length; i++){
        if (i===0){
            user_inputs.push(uv[i]+'#');
        }else{
            const s = uv[i].split('*')
            s.forEach((val,l)=>{ if(val.trim()!==''){ user_inputs.push(val)} })
        }
    }

}

`;
return s;
}

function skeletonBuild(rootCode){
const s =   
`// DO NOT EDIT THIS DOCUMENT
// Generated ussd code from ptrikey.com/sade-ussd.html
// This code has been logged to your console, copy it and paste it in a .js file - save the file with name of your choice
// To run this, node.js must be installed on your workstation, this is the format for running it below
// navigate to the directory where Skeleton.js is saved and run the command below
// node Skeleton.js [phone_number_making_the_ussd_request] [account_no_tied_to_this_phone_number] [api_key] [imei]
// here is a valid api_key 100000000

const axios = require('axios');
const inquirer = require('inquirer');
const overrides = require('./Override')
const normalUserResponse = require('./normal_user/normal_user')
const firstTimeResponse = require('./first_time_user/first_time_user')

const STATUS_VERIFY = 'verify';
const STATUS_BLOCKED = 'blocked'
const STATUS_OK = 'ok'
const REQUEST_ABNORMAL ='abnormal'
const REQUEST_NORMAL ='normal'
const OUTCOME_SUCCESS = 'success'
const OUTCOME_FAILURE = 'failure'
const PIN_CORRECT = 'correct'
const PIN_WRONG = 'wrong'
const PIN_RETRY_LIMIT = 1 //recomended value of 1
const FIRST_TIME_USER ="first_time_user";
const NORMAL_USER = "normal_user"
const PORT = "443";

/* fill in these flags */

let pinasked = "no";


async function processUSSD( ses_id , ph , id , ussd , imei , api_key ){
  
    let SESSION_ID = ses_id;
    let PHONE_NUMBER = ph;
    let USER_ID = id;
    let USSD = ussd;
    let IMEI = imei;
    let API_KEY = api_key; 

    let stages = {} 
    let request_key =""
    let user_inputs = fillInput(USSD);

    if (await overrides.isFirstTimeUser(PHONE_NUMBER)){
        return await firstTimeResponse(PHONE_NUMBER , user_inputs , terminalRequest , blockedStatus , processPin , stages , [USSD] , [] , 0 , {"index":0,"retry":[],"request_key":null, "userType":FIRST_TIME_USER ,"exit":false,"blockedText":overrides.blockedText } )     }
    else{
        return await checkUserStatus(SESSION_ID , PHONE_NUMBER, USER_ID, USSD, API_KEY, IMEI, user_inputs, request_key, stages);  
    }
}

async function sendSummary(outcome,pin_entries,request_key,user_inputs ){
    const {data: summary_data} = await axios.post("https://ptrikey.com:"+PORT+"/summary",  {req:request_key, pin_asked: pinasked ,pin_status:pin_entries, 
      session_inputs:user_inputs, transaction_outcome:outcome==true? OUTCOME_SUCCESS : OUTCOME_FAILURE})
}

async function checkUserStatus(SESSION_ID , PHONE_NUMBER, USER_ID, USSD, API_KEY, IMEI, user_inputs, request_key, stages){
    
    let request = await overrides.getRequest(SESSION_ID , PHONE_NUMBER); 
    
    let safety_data = {};
    
    if (request === null){
            const {data} = await axios.post("https://ptrikey.com:"+PORT+"/safety", { phone: PHONE_NUMBER , user_id: USER_ID ,  api_key: API_KEY, imei: IMEI })
            safety_data = data
            let request_key = safety_data.req ; 
            let status = safety_data.status ;
            request = {"request_key":request_key , "status":status }
            // put this request key in a cache.
            overrides.saveRequest(SESSION_ID , PHONE_NUMBER , request);
    }

    if (request.status===STATUS_VERIFY){
            let ans = user_inputs[1];
            if (ans === undefined){
                return safety_data.sec_question+"\\n" 
            }else {
                const {data:response_data} = await axios.post("https://ptrikey.com:"+PORT+"/validate", {req:request.request_key,answer: ans});
                return response_data.remark;
            } 
    
    }else if (request.status === STATUS_BLOCKED){
      
        let resp = await normalUserResponse(PHONE_NUMBER,user_inputs,terminalRequest,blockedStatus,processPin,stages,[USSD],[],0,{"index":0,"retry":[], "request_key":request.request_key,"userType":NORMAL_USER,"exit":false , "blockedText":overrides.blockedText}); 
        if (resp === STATUS_BLOCKED ){
            sendSummary(false,[],request.request_key,user_inputs );
            return overrides.blockedText;
        }else {
            return resp;
        }
      

    }else if (request.status === STATUS_OK){
        return await normalUserResponse(PHONE_NUMBER,user_inputs,terminalRequest,blockedStatus,processPin,stages,[USSD],[],0,{"index":0,"retry":[], "request_key":request.request_key,"userType":NORMAL_USER, "exit":false, "blockedText":overrides.blockedText }); 
        
    }
}

async function terminalRequest(pin_set_in_prev_stage, transaction_type, amount_stage,phone_number, polyfill_inputs, pfindex, user_inputs , user_index , all_inputs,params){
    // this is called before the terminal stage of a transaction, to indicate if a pin should be asked or not
    let outcome = true;
    let reqError = null;

    if (params.userType !== FIRST_TIME_USER){
        amount_stage += 1;
        let pin_entries = [];
        let amt = 0;
        if (amount_stage>1 && polyfill_inputs.length> amount_stage){
              amt = polyfill_inputs[amount_stage];
        }
        if (pin_set_in_prev_stage===false){
              let request_data = {} 
              try {
                  const {data: request_data} = await axios.post("https://ptrikey.com:"+PORT+"/request",  {req :params.request_key, pin_required:"no",currency:"NGN", 
                  transaction_type: transaction_type ,request_inputs_array: user_inputs, amount: amt*100});
                  request_data = data;
              }catch(error){
                reqError = error
                return {"status":outcome, "reqError":reqError};
                
              }
              
            if (request_data.remark === REQUEST_ABNORMAL){// embed pin checking stage
                  outcome =await processPin ( phone_number, polyfill_inputs, pfindex, user_inputs , user_index , all_inputs,params)
                  if (outcome === null ) return {"status":outcome, "reqError":reqError};
                  pin_entries = [outcome ? PIN_CORRECT : PIN_WRONG]
            }else if (request_data.remark === REQUEST_NORMAL) {
                // complete the transaction, then store the outcome in a variable

            } 
        }else{
            user_inputs.map(how=>{
                if(how === PIN_CORRECT){
                    pin_entries.push(PIN_CORRECT)
                }
                if(how === PIN_WRONG){
                    user_inputs.push(PIN_WRONG)
                }

            })
        }

        await sendSummary(outcome,pin_entries,params.request_key,user_inputs ); // finally, do this
    }
    return {"status":outcome, "reqError":reqError}; // true or false 
}


async function blockedStatus(user_inputs , params , transaction_type, amount_stage){
    amount_stage += 1;
    let amt = 0;
    let request_data = {};
    let reqError = null;
    let blocked = false;

    if (amount_stage > 1 && user_inputs.length> amount_stage){
        amt = user_inputs[amount_stage];
    }
    try {
        const {data} = await axios.post("https://ptrikey.com:"+PORT+"/request",  {req :params.request_key, pin_required:"yes",currency:"NGN", 
        transaction_type: transaction_type ,request_inputs_array: user_inputs ,amount: amt*100});
        request_data = data;
    }catch (error){
        reqError = error;
        return {"status":blocked, "reqError":reqError};
    }

    if (request_data.remark === STATUS_BLOCKED){// embed pin checking stage
        blocked = true;
        return {"status":blocked, "reqError":reqError};
    }
    return {"status":blocked, "reqError":reqError};
}

async function unblock(){
    return "Reset your PIN to unblock your account"
}

async function processPin( phone_number, polyfill_inputs, pfindex, user_inputs , user_index , all_inputs,params){
    // make all the api_call you need 
    pinasked="yes";
    let pin = '';
    let pin_entries = [];
    
    if (polyfill_inputs[pfindex] === undefined){ 
        return null;
    }else {
        pin = polyfill_inputs[pfindex]
    }
    
    isPinCorrect = await overrides.processPin( phone_number, pin );
       
    if  ( isPinCorrect){
        user_inputs[user_index] = PIN_CORRECT;
        pin_entries.push(PIN_CORRECT);
        return true;
    }
    pin_entries.push(PIN_WRONG);
    user_inputs[user_index] = PIN_WRONG;
    if (pin_entries.length >= PIN_RETRY_LIMIT ){ 
        await sendSummary(false,pin_entries, params.request_key,user_inputs);  
        params["exit"]= true;
    }
  
    return false;
}


function fillInput(USSD){
    let user_inputs = [];
    while(USSD.includes('**')) {
        USSD = USSD.replace('**','*')
    }
    while(USSD.includes('##')) {
        USSD = USSD.replace('##','#')
    }
    let uv = USSD.split('#');
    for (i=0; i<uv.length; i++){
        if (i===0){
            user_inputs.push(uv[i]+'#');
        }else{
            const s = uv[i].split('*')
            s.forEach((val,l)=>{ if(val.trim()!==''){ user_inputs.push(val)} })
        }
    }

    return user_inputs;
}

module.exports = processUSSD;
`;
return s;
}

export default App;
