import React, {useState} from 'react';
import Column from './Column';

function Opt({ci , oi , columns, setColumns, drg, setDrag,cap,u_type}){
	const [code, setCode] = useState ({view:false})
  const getBorderColor = (flowType)=>{
    if (flowType === "menu")   return '0.2rem solid #55ae95';
    if (flowType === "amount") return '0.2rem solid #89a0da';
    if (flowType === "text")  return  '0.2rem solid #fd7792';
    if (flowType === "pin")   return '0.2rem solid #ffac8e';
    if (flowType === "end")   return '0.2rem solid #9e9a9a';
    return null;
  }
	const col = columns[ci].opt[oi];
	return (
			<div  style= {{ backgroundColor:"#efefef", display:"flex", flexDirection:"column", margin:"0.3rem", padding:"0.3rem", borderRadius:"0.3rem",minWidth:"8rem",borderTop:getBorderColor(col.flowType) }}
                    className={ci+"s"+oi}  onDragOver = { (e) => {  e.preventDefault(); /*console.log("drag over "+ci+"s"+oi)*/} } 
                    onDrop = { (e) => { e.preventDefault(); const nDrgObj = {...drg};
                      nDrgObj.stopElem = ci+"s"+oi; setDrag(nDrgObj);  } } >
                      <div style={{display:"flex", justifyContent:"flex-end", marginBottom:"0.3rem",minHeight:"1rem" }}> 
	                      <input type="text" value={col.quickCode} placeholder="quickCode" size="30" style={{flexGrow:"1",margin: "0", backgroundColor:"#e6e3e3",fontWeight:"normal", fontSize:"0.75rem",border:"none"}} 
                          onChange = {(e)=>{
                            // stagename must fit this regex pattern
                            const newCols = [...columns]; newCols[ci].opt[oi].quickCode= e.target.value; setColumns(newCols)
                          }}/>
                        <button type="button" style={{fontSize:"0.6rem", borderRadius:"3px", border:"1px solid #989191"}} 
	                        onClick= { (e) => { e.preventDefault(); const nCode = {...code};
	                        nCode.view = !nCode.view; setCode(nCode);  } }>{!code.view? "code" : "design"} </button> 

                      </div>
                     
                    <input type="text" value={col.stageName} placeholder="Title" size="20" style={{margin: "0",marginBottom:"0.2rem", backgroundColor:"#e6e3e3",fontWeight:"bold", fontSize:"0.9rem",border:"none"}} 
                      onChange = {(e)=>{
                        // stahgename must fit this regex patter
                        if(/^[a-zA-z_]*[a-zA-z0-9_]+$/.test(e.target.value)){
                          //stagename must be unique to the stage
                          let uniq = true;
                          columns.filter(col=>col.opt.some(op  =>{ if (op.stageName === e.target.value) {uniq =false}   }));
                          //console.log(uniq+' unique ')
                          if (uniq){
                            const newCols = [...columns]; newCols[ci].opt[oi].stageName= e.target.value.toLowerCase(); setColumns(newCols)
                          }
                        }
                      }}/>
                    
                    
                    {code.view ? (<div style={{display:"flex", flexDirection:"column"}}>
                    	<textarea rows="4" style={{color:"white", backgroundColor:"#3a3939",outline:"none", border:"none",resize:"none"}} value ={'hook['+ci+']['+oi+'] = '+cap.pre} 
                    	onFocus = {(e)=> document.getElementById("hook"+ci+""+oi).focus()}
                    	/>
                    	<textarea id = {"hook"+ci+""+oi} rows = "15" cols="60" style={{color:"white", backgroundColor:"#3a3939",outline:"none", border:"none"}} autoFocus
                    	value={ col.hook}
                    	onChange = {(e)=>{ 
                    			const newCols = [...columns];
								          newCols[ci].opt[oi].hook = e.target.value
                    			setColumns(newCols);
                    	}}/>
                    	<textarea rows="4" style={{color:"white", backgroundColor:"#3a3939",outline:"none",border:"none",resize:"none"}} value ={cap.post} 
                    	 onFocus = {(e)=> document.getElementById("hook"+ci+""+oi).focus()}/>
                    	</div>
                    	) : ( col.flowType !== "" ? (
                      <React.Fragment>
                        <input type="text" value={col.ins} placeholder="Instruction" size="20"
                          style={{marginBottom:"0.4rem",border:"none", backgroundColor:"#e6e3e3"}} onChange = {(e)=>{const newCols = [...columns]; newCols[ci].opt[oi].ins= e.target.value; setColumns(newCols)}}/> 
                        { col.flowType === "amount" ? <div style={{display:"flex", flexDirection :"column", marginBottom :"0.4rem"}}> <label style={{fontSize:"0.8rem",textAlign:"center"}}> Range  </label>  <hr style={{margin:"0",padding:"0",marginTop:"0.2rem"}}/></div> : null }
                    
                        <Column  upd={setColumns} cols={columns} index={ci} optIndex={oi} drgObj={drg} setDrgObj={setDrag} /> 
                      </React.Fragment> 
                      ):(
                      <div> 
                        <h4> Select input type </h4>
                        <div onClick={()=>{const newCols = [...columns]; newCols[ci].opt[oi].flowType= "menu"; setColumns(newCols)}} style={{display:"flex", flexDirection:"column", alignItems:"center", maxWidth:"9rem", backgroundColor:"#55ae95", color:"white", padding:"0.2rem"}}>
                          <label style={{textDecoration:"underline"}}> Menu </label>
                          <label style={{fontSize:"0.8rem", textAlign:"center"}}> Show a Ussd Menu which the user can choose from </label>
                        </div>
                        <div onClick={()=>{const newCols = [...columns]; newCols[ci].opt[oi].flowType= "amount"; newCols[ci].opt[oi].amountInputStage=ci; setColumns(newCols)}} style={{display:"flex", flexDirection:"column", alignItems:"center", maxWidth:"9rem", backgroundColor:"#3f4d71", color:"white", padding:"0.2rem"}}>
                          <label style={{textDecoration:"underline"}}> Amount </label>
                          <label style={{fontSize:"0.8rem",textAlign:"center"}}> The User is expected to input an amount </label>
                        </div>
                        <div onClick={()=>{const newCols = [...columns]; newCols[ci].opt[oi].flowType= "text"; newCols[ci].opt[oi].textInputStage=ci; setColumns(newCols)}} style={{display:"flex", flexDirection:"column", alignItems:"center",maxWidth:"9rem", backgroundColor:"#fd7792", color:"white", padding:"0.2rem"}}>
                          <label style={{textDecoration:"underline"}}> Text </label>
                          <label style={{fontSize:"0.8rem",textAlign:"center"}}> For inputing textual data like Account Number, Phone number... etc </label>
                        </div>
                        {u_type!=="first_time_user"?<div onClick={()=>{const newCols = [...columns]; newCols[ci].opt[oi].flowType= "pin";  newCols[ci].opt[oi].stageName="authentication_stage"; setColumns(newCols)}} style={{display:"flex", flexDirection:"column", alignItems:"center",maxWidth:"9rem", backgroundColor:"#ffac8e",color:"white", padding:"0.2rem"}}>
                          <label style={{textDecoration:"underline"}} > PIN</label>
                          <label style={{fontSize:"0.8rem", textAlign:"center"}}> The User is expected to input his PIN </label>
                        </div>:null}
                        <div onClick={()=>{const newCols = [...columns]; newCols[ci].opt[oi].flowType= "end";  newCols[ci].opt[oi].flows=[]; newCols[ci].opt[oi].stageName="Final Message"; setColumns(newCols)}} style={{display:"flex", flexDirection:"column", alignItems:"center",maxWidth:"9rem", backgroundColor:"#312f2f",color:"white", padding:"0.2rem"}}>
                          <label style={{textDecoration:"underline"}} > END</label>
                          <label style={{fontSize:"0.8rem", textAlign:"center"}}> Display a message to indicate session has ended </label>
                        </div>
                      </div> )
                    )}
            </div>
                  
                          
 		)
}

export default Opt;