import React from 'react'

function PlayButton({style, onClick, visible}){
	
	return (
		<div style={style} onClick={onClick}>{ 
    			visible ? 
    			<svg version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style={{enableBackground:"new 0 0 512 512", fill:"white"}} 
	    			
           		>
      				<g>
  						<g>
    <path d="M435.078,209.327L140.047,9.81c-17.57-11.883-39.249-13.04-57.984-3.092c-18.737,9.948-29.922,28.551-29.922,49.764
      v399.034c0,21.213,11.186,39.816,29.922,49.764C90.52,509.772,99.577,512,108.585,512c10.948,0,21.824-3.292,31.463-9.81
      l295.03-199.516c15.517-10.494,24.781-27.941,24.781-46.673S450.595,219.821,435.078,209.327z"/>
  						</g>
					</g>
				</svg> : null}
    		</div> 
	)
}

export default PlayButton