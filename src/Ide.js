import React,{useState} from 'react';
const fs = window.require('fs');

function Ide({backingFile, coding, setCoding, updateParam,u_type}){
	
	const [stuff,setStuff] = useState({saved:true})

	return(
	<div style={{display:"flex", flexDirection:"column", flexGrow:"1",overflow:"hidden"}}> 
		<div style={{display:"flex", minHeight:"1.8rem"}}>
            {
              ['Skeleton.js','Hooks.js','Override.js',u_type].map((title,i) => (
                <div key={i} style={{backgroundColor:coding.tab===title?"#3a3939":"#6d6868", padding:'0.3rem',color:"white", display:"flex", alignItems:'center',border:"none",outline: "none"}} onClick= {(e)=> {const nCoding = {...coding}; nCoding.tab= title; setCoding(nCoding);}}>
                  <label style={{fontSize:'0.9rem'}}>{title}</label> 
                  {
                  	!stuff.saved &&coding.tab===title ?
                  <label style={{width:'0.3rem', height:'0.3rem', backgroundColor:"#b5b5b5", borderRadius:'10rem', marginLeft:'0.3rem'}}> </label>
                  : null
              	  }
                </div>
              ))
            }
            
         </div>
		<pre style={{ display:"flex", flexGrow:"1", margin:"0",paddingLeft:"1rem", backgroundColor:"#3a3939"}}>
                <div contentEditable autoFocus style={{flexGrow:"1", color:"white", margin:"0",overflow:"auto",outline: "none"}}
                  onInput ={(e)=>{ 
                  	const nStuff= {...stuff};
                  	nStuff.saved = false;
                  	setStuff(nStuff);
                  }}

                  onBlur={(e)=>{
                    e.stopPropagation();
                    let ntext = e.target.innerText;
                    const nCoding = {...coding}
                    nCoding[updateParam] = ntext;
                    setCoding(nCoding);
                    fs.writeFileSync( backingFile,ntext,'utf8',function (err) { if (err) return alert(err);} )
                    const nStuff= {...stuff};
                  	nStuff.saved = true;
                  	setStuff(nStuff);
                    
                  }}

                  onFocus={(e)=>{
                    e.stopPropagation();
                    const nCoding = {...coding}; 
                    nCoding[updateParam]= fs.readFileSync(backingFile, 'utf8');
                    setCoding(nCoding);
                  }}>
                  { coding[updateParam]}</div>
        </pre>
    </div>
		);
}

export default Ide