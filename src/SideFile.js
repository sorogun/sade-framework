import React,{useState} from 'react';
import PlayButton from './PlayButton';
const fs = window.require('fs');

function SideFile ({file_name, dirPath, isDir,setDrag,level}){
const [files, setFiles] = useState([])
const left_pad = level* 1;
	return (
		
	<div style={{display:"flex", flexDirection:"Column", paddingLeft:left_pad+"rem"}}>
		<div style={{display:"flex",alignItems:"center"}}>  
    		
    		<PlayButton style={{width:"0.5rem", marginRight:"0.1rem"}} visible = {isDir} onClick={()=>{
	    				var nFiles = fs.readdirSync(dirPath);
		            	setFiles(nFiles);
	            	}}/> 

    		<label style={{color:"white", fontSize:"0.8rem"}}>{file_name}</label> 
    			
    		
    	</div>
    <React.Fragment>
    	{ ( ()=>{ 	
	    			return files.map((file,i)=>{
	    			let is_dir = fs.lstatSync(dirPath+'/'+file).isDirectory();
	    			return <SideFile key={i}  file_name={file} dirPath={isDir?dirPath+"/"+file: dirPath} isDir={is_dir} setDrag={setDrag} level={level+1} /> })	    		
	    		})()
	    }
    </React.Fragment>

    </div>)
}

export default SideFile