const path = require('path')
const {app, BrowserWindow} = require('electron')      
var fs = require('fs');

function createWindow () {   
  // Create the browser window.     

win = new BrowserWindow({width: 900, height: 700, webPreferences: { nodeIntegration: true }}) 
win.on('close', function(e){
    var choice = require('electron').dialog.showMessageBox(this,
    	{
        	type: 'question',
          	buttons: ['Yes', 'No'],
          	title: 'Confirm',
          	message: 'Are you sure you want to quit?'
       });
       if(choice == 1){
         e.preventDefault();
       }
 });
 
// and load the index.html of the app.     
//win.loadFile('index.html')   

//win.loadURL('http://localhost:3000/')
win.loadURL(`file://${path.join(__dirname, '../build/index.html')}`)
}      
app.on('ready', createWindow)